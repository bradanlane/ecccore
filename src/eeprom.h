/* ************************************************************************************
* File:    eeprom.h
* Date:    2020.05.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## eeprom.h - read/write interface to ATMega4809 eeprom memory

The module provides basic read/write of the microcontroller eeprom.

**Tip:** Writing a specific bytecode to a specific location of the EEPROM is an easy way to know if
it has been used before or if initial data should be written. If the bytecode is missing, then it can be
considered "unformatted" and any initial data should be written.
Alternately, you could test the values of every read operation.
See the `config.h` for additional details.

--------------------------------------------------------------------------
--- */

#define EEPROM_MIN 0x0000 // 0KB
#define EEPROM_MAX 0x0100 // 256bytes - YIKES!

#define EEPROM_FORMAT_MARKER_BYTE		0xAB	// any byte code other than 0x00 and 0xFF

// all of the following are unsigned byte data entries in the eeprom space
#define EEPROM_FORMAT_MARKER  		0x0000 // any location less than the MAX of the microcontroller EEPROM space
#define EEPROM_VERIFIED		  		0x0001 // After the smoketest has been verified, this is non-zero
#define EEPROM_DEMO_MODE	  		0x0002 // the touch+led demo which is active (when non-zero it overrides active game)
#define EEPROM_GAME_LEVEL	  		0x0003 // the touch+led game which is active 1..3 (4 = done)
#define EEPROM_GAME_BONUS	  		0x0004
#define EEPROM_GAME_BONUS_SPECIAL	0x0005
#define EEPROM_STORY_LEVEL	  		0x0006 // the story puzzle which is active 1..3 (4 = done)
#define EEPROM_STORY_POSITION 		0x0007 // the user's current line number from the story
#define EEPROM_TOUCH_CALIBRATION	0x0010 // up to 16 touch sensors; if 8 or less, then they could go in the 0x0000-0x0010 block
#define EEPROM_ID_STORAGE	  		0x0020 // first slot used for storing received coin IDs (0x0100 - 0x0020 = 224; IDs are 6 bytes so 224 / 6 = 37 IDs storage

#define DEVICE_ID_SIZE 6 // we are only interested in 6 bytes of the MCU ID

BOOL _eeprom_formatted; // this is mostly for diagnostics so we can query if we reset the eeprom during startup


/* ---
uint8_t eepromIsReady() - returns TRUE if the eeprom is ready and FALSE if it busy
--- */
uint8_t eepromIsReady() {
	if (NVMCTRL.STATUS & NVMCTRL_EEBUSY_bm)
		return FALSE;
	return TRUE;
}

/* ---
void eepromWriteByte() - write a single byte to an eeprom memory location
--- */
void eepromWriteByte(uint16_t relative_addr, uint8_t data) {
	if (relative_addr < EEPROM_MAX) {

		/* Wait for completion of previous write */
		while (NVMCTRL.STATUS & NVMCTRL_EEBUSY_bm)
			;

		// Load in the relevant EEPROM page by writing to the address
		*(uint8_t *)(EEPROM_START + relative_addr) = data;

		// Unlock self programming and then erase/write the page
		// - should only erase the one byte
		CCP = CCP_SPM_gc;

#ifdef CHIP_ATMEGA4809
		NVMCTRL.CTRLA = NVMCTRL_CMD_PAGEERASEWRITE_gc;
#endif
#ifdef CHIP_AVR64DA48
		AVR64DA48 NVMCTRL.CTRLA = NVMCTRL_CMD_EEERWR_gc;
#endif
	}
}

/* ---
void eepromReadByte() - read a single byte from eeprom memory relative to the eeprom base address
--- */
uint8_t eepromReadByte(uint16_t relative_addr) {
	uint8_t b = 0;
	if (relative_addr < EEPROM_MAX) {
		// Read operation will be stalled by hardware if any write is in progress
		b = *(uint8_t *)(EEPROM_START + relative_addr);
	}
	return b;
}

/* ---
void eepromFormat() - set a marker byte to indicate the EEPROM has a known default state
--- */
void eepromFormat() {
	eepromWriteByte(EEPROM_FORMAT_MARKER, EEPROM_FORMAT_MARKER_BYTE);
	eepromWriteByte(EEPROM_VERIFIED, 0);
	eepromWriteByte(EEPROM_DEMO_MODE, 0);
	eepromWriteByte(EEPROM_GAME_LEVEL, 1);
	eepromWriteByte(EEPROM_GAME_BONUS, 0);
	eepromWriteByte(EEPROM_GAME_BONUS_SPECIAL, 0);
	eepromWriteByte(EEPROM_STORY_LEVEL, 1);
	eepromWriteByte(EEPROM_STORY_POSITION, 0);
	_eeprom_formatted = TRUE;
}

/* ---
BOOL eepromWasFormatted() - return TRUE if the eeprom was formatted during the current boot/execution
--- */
BOOL eepromWasFormatted() {
	return _eeprom_formatted;
}

/* ---
BOOL eepromInited() - return TRUE when the EEPROM is ready for use
--- */
BOOL eepromIsInited() {
	return TRUE;
}

/* ---
void eepromInit() - initialization of the EEPROM functions and conditionally format
--- */
void eepromInit() {
#ifdef EEPROM_FORMAT_MARKER_BYTE
	if (eepromReadByte(EEPROM_FORMAT_MARKER) != EEPROM_FORMAT_MARKER_BYTE) {
		eepromFormat();
	}
#endif
}

/* ---
uint8_t* eepromSignature() - returns a pointer to a static buffer with our version the MCU unique ID
--- */

uint8_t *eepromSignature() {
	static uint8_t sig[6];
	/* Examples
	SIGNUM:  0  1 2  3 4 5  6 7 8 9
	   UID: 51 5052 384E20 8AEF3C16
	   UID: 51 5052 384E20 240C200E
	   UID: 51 5231 435720 85E62D16
	*/
	// need more testing but for now we will take 1,2,6,7,8,9

	sig[0] = SIGROW.SERNUM1;
	sig[1] = SIGROW.SERNUM2;
	sig[2] = SIGROW.SERNUM6;
	sig[3] = SIGROW.SERNUM7;
	sig[4] = SIGROW.SERNUM8;
	sig[5] = SIGROW.SERNUM9;
	return sig;
}

/* ---
int eepromAddID() - store a chip ID but only if it has not already been stored
--- */

int eepromAddID(uint8_t *new_code) {
	if (new_code == NULL)
		return -1;

	// we need to check if we have already received this ID
	uint8_t bonus_count = eepromReadByte(EEPROM_GAME_BONUS);
	if (bonus_count == 0xFF) bonus_count = 0;	// sanity check in case the location has not been initialized
	uint16_t slot_start = 0;
	uint8_t matched = FALSE;

#ifndef IR_CHEAT
	uint8_t *our_code = eepromSignature();
	matched = TRUE;
	// check if we are receiving our own ID
	for (uint8_t i = 0; i < DEVICE_ID_SIZE; i++) {
		if (new_code[i] != our_code[i]) {
			matched = FALSE; // the current slot is not a match
			break;
		}
	}

	if (matched) { // we found ourselves ?!
		return -2;
	}
#endif

	for (uint8_t slot_num = 0; slot_num < bonus_count; slot_num++) {
		slot_start = EEPROM_ID_STORAGE + (slot_num * DEVICE_ID_SIZE);

		matched = TRUE; // assume we will find a match
		for (uint8_t i = 0; i < DEVICE_ID_SIZE; i++)
			if (new_code[i] != eepromReadByte(slot_start + i))
				matched = FALSE; // the current slot is not a match
		if (matched)			 // no need to continue if we found a match
			break;
		matched = FALSE; // we assume not a match in case the for loop is done
	}

	if (matched)		// we found a duplicate
		return -1;

	slot_start = EEPROM_ID_STORAGE + (bonus_count * DEVICE_ID_SIZE);

	// we want to avoid running off the end of the eeprom space
	if ((slot_start + DEVICE_ID_SIZE) >= EEPROM_MAX)
		return 0;

	for (uint8_t i = 0; i < DEVICE_ID_SIZE; i++)
		eepromWriteByte(slot_start + i, new_code[i]);

	bonus_count++;
	eepromWriteByte(EEPROM_GAME_BONUS, bonus_count);

	return bonus_count;
}
