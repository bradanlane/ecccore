/* ************************************************************************************
* File:    flash.h
* Date:    2020.05.04
* Author:  Bradan Lane Studio
*
* Created: 4/11/2012 12:30:00 PM
 * Authors: Michael Wu (myw9) and Garen Der-Khachadourian (gdd9)
 * Description: Library for SPI communication to perform FLASH memory operations
 * https://people.ece.cornell.edu/land/courses/ece4760/FinalProjects/s2012/myw9_gdd9/myw9_gdd9/software/flash.c
 *
 * alternate material:
 * https://people.ece.cornell.edu/land/courses/ece4760/FinalProjects/s2012/shw46_jec324/shw46_jec324/files/code/spiflash.c
 * 
 * This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## flash.h - format, read/write interface to SPI FLASH storage

The module provides basic formatting, read, and write to SPI connected FLASH storage.

The code was developed and tested with the WINBOND FLASH Chip: W25Q64BVSIG (older cheaper slower). This chip has 
8MB of total storage broken into 256Byte pages. The chip is access using 4KB sectors. The chip is formatted either in 
its entirety or at the 64KB block level or 4KB sector level.

**Note:** To overwrite a page the entire sector (or block or chip) must be erased first.

**Warning:** This code requires a 256 byte buffer for page level operations.
The code establishes `_flash_page_buffer` as a local buffer. To save data space, 
it is recommended to treat this as a read-only buffer within your code.

--------------------------------------------------------------------------
--- */

#if !FLASH_ENABLED

#define flashWriteData(block, page, index, buffer, size) {FALSE}
#define flashReadData(block, page, index, buffer, size, just_text) {0}
#define flashReadByte(block, page, index) {0}
#define flashFormat() {FALSE}
#define flashIsFormatted() {FALSE}
#define flashIsInited() {FALSE}
#define flashWasFormatted() {FALSE}
#define flashInit() {}

#else

/******************************************************************************
 * Constants
 *****************************************************************************/

#define CHIP_TOTAL_SIZE		8388608L
#define CHIP_BLOCK_SIZE		65536L
#define CHIP_SECTOR_SIZE	4096
#define CHIP_PAGE_SIZE		256

#define CHIP_BLOCKS_IN_CHIP		(CHIP_TOTAL_SIZE / CHIP_BLOCK_SIZE)		// 0x080 128 blocks on the chip
#define CHIP_SECTORS_IN_BLOCK	(CHIP_BLOCK_SIZE / CHIP_SECTOR_SIZE)	// 0x010  16 sectors in a block
#define CHIP_PAGES_IN_SECTOR	(CHIP_SECTOR_SIZE / CHIP_PAGE_SIZE)		// 0x010  16 pages in a sector
#define CHIP_PAGES_IN_BLOCK		(CHIP_BLOCK_SIZE / CHIP_PAGE_SIZE)		// 0x100 256 pages in a block

#define DUMMY_DATA 0xFF

// Flash Instruction Codes
#define PAGE_PROG 				0x02
#define READ_DATA 				0x03
#define WRITE_ENABLE 			0x06

#define READ_STATUS_REGISTER1	0x05
//#define READ_STATUS_REGISTER2	0x35
#define READ_MANUFACTURER		0x90

#define POWER_DOWN				0xB9
#define RELEASE_POWER_DOWN		0xAB

#define CHIP_ERASE				0x60
//#define CHIP_ERASE			0xC7
#define BLOCK_ERASE				0xD8	// 64k block
#define SECTOR_ERASE			0x20	// 4K sector

// some useful return codes to test for
#define WRITE_ENABLE_FLAG		0x02
#define FLASH_BUSY_FLAG			0x01


#include <avr/io.h>

BOOL _flash_inited; 						// keep track of the existence of the Flash chip
BOOL _flash_formatted;						// this is mostly for diagnostics so we can query if we formatted during startup

uint8_t _flash_page_buffer[CHIP_PAGE_SIZE];



/******************************************************************************
 * localc SPI Functions
 *****************************************************************************/

// Enable slave (CS pin)
void _spi_enable_slave(void) {
	FLASH_PORT &= ~(1 << FLASH_CS_PIN);
}

// Disable slave (CS pin)
void _spi_disable_slave(void) {
	FLASH_PORT |= (1 << FLASH_CS_PIN);
}

// Transmit byte via SPI
uint8_t _spi_tx(uint8_t data) {
	SPDR0 = data;
	while (!(SPSR0 & (1 << SPIF)))
		_delay_us(10);
	return SPDR0;
}

// Send 24-bit address
void _spi_tx_address(uint8_t block, uint8_t page, uint8_t index) {

	_spi_tx(block);
	_spi_tx(page);
	_spi_tx(index);
}

/******************************************************************************
 * local Flash Memory Functions
 *****************************************************************************/

// Send "Read Status Register-1" Command
uint8_t _flash_cmd_read_status_register(void) {
	uint8_t sr1 = 0;
	_spi_enable_slave();
	_spi_tx(READ_STATUS_REGISTER1);
	sr1 = _spi_tx(DUMMY_DATA);
	_spi_disable_slave();
	return sr1;
}

// Wait until Flash is not busy
void _flash_wait_ready(void) {
	uint8_t sr1 = FLASH_BUSY_FLAG;
	while (sr1 & FLASH_BUSY_FLAG) {
		sr1 = _flash_cmd_read_status_register();
		_delay_us(10);
		//_delay_ms(1);
	}
}

// Send "Write Enable" Command
BOOL _flash_cmd_write_enable(void) {
	uint8_t sr1 = 0;

	// try up to 10 times to get the write enable
	for (uint8_t i = 0; i < 10; i++) {
		_flash_wait_ready();
		_spi_enable_slave();
		sr1 = _spi_tx(WRITE_ENABLE);
		_spi_disable_slave();
		if (sr1 == WRITE_ENABLE_FLAG)
			return TRUE;
		//_delay_us(10);
		_delay_ms(1);
	}
	return FALSE;
}

// Send "Chip Erase" Command
uint8_t _flash_cmd_erase_chip(void) {
	uint8_t rtn = _flash_cmd_write_enable();	// bool TRUE is successful
	//_flash_wait_ready();
	_spi_enable_slave();
	_spi_tx(CHIP_ERASE);
	_spi_disable_slave();
	//_flash_wait_ready();
	return rtn;
}

// Send "64KB Block Erase" Command
uint8_t _flash_cmd_erase_64kb(uint8_t block_num) {
	uint8_t rtn = _flash_cmd_write_enable();	// bool TRUE is successful
	//_flash_wait_ready();
	_spi_enable_slave();
	_spi_tx(BLOCK_ERASE);
	_spi_tx_address(block_num, 0, 0);
	_spi_disable_slave();
	_flash_wait_ready();
	return rtn;
}
#if 0
// Send "Sector Erase" Command
uint8_t _flash_cmd_erase_4kb(uint8_t block, uint8_t sector) {
	uint8_t rtn = _flash_cmd_write_enable();	// bool TRUE is successful
	//_flash_wait_ready();
	_spi_enable_slave();
	_spi_tx(SECTOR_ERASE);
	_spi_tx_address(block, sector * CHIP_PAGES_IN_SECTOR, 0);
	_spi_disable_slave();
	//_flash_wait_ready();
	return rtn;
}
#endif

// Send "Read Data" Command
void _flash_cmd_read_device(uint8_t *buffer, uint16_t size) {
	uint16_t i = 0;
	if (size < 6)
		return;
	_flash_wait_ready();
	_spi_enable_slave();
	_spi_tx(READ_MANUFACTURER);
	for (i = 0; i < 5; i++) {
		buffer[i] = _spi_tx(DUMMY_DATA);
	}
	_spi_disable_slave();
}



/******************************************************************************
 * public Flash Memory Functions
 *****************************************************************************/

/* ---
BOOL flashWriteData(uint16_t block, uint16_t page, uint16_t index, uint8_t *buffer, uint16_t size) - 
write data to a page of FLASH storage at the specified block+page+index address. The size of the write must be 
less than a full page - eg 256 bytes - and most not attempt to write across a page boundary.

The BLock+Page+Index address indicates a 64 KB block, a 256 Byte Page within that block, and then an index (usually 0) withing that page.

*Kludge:* the write seems to have a timing issue beyond about 238 bytes
--- */
BOOL flashWriteData(uint16_t block, uint16_t page, uint16_t index, uint8_t *buffer, uint16_t size) {
	if (buffer == NULL)
		return FALSE;

	if ((size + index) >= CHIP_PAGE_SIZE)
		return FALSE;

	uint16_t i = 0;

	_flash_cmd_write_enable();
	_flash_wait_ready();
	_spi_enable_slave();
	_spi_tx(PAGE_PROG);
	_spi_tx_address(block, page, index);

	for (i = 0; i < size; i++) {
		_spi_tx(buffer[i]);
	}
	// null terminate string is possible
	if (size < CHIP_PAGE_SIZE)
		_spi_tx(0);

	_spi_disable_slave();
	return TRUE;
}

/* ---
int16_t flashReadData(uint16_t block, uint16_t page, uint16_t index, uint8_t *buffer, uint16_t size, BOOL just_text) - 
read data from a page of FLASH storage from the specified block+page+index address. The size of the read must be 
less than a full page - eg 256 bytes - and most not attempt to read across a page boundary.

The Block+Page+Index address indicates a 64 KB block, a 256 Byte Page within that block, and then an index (usually 0) withing that page.

The `just_text` flag indicates that the read operation should stop if it encounters an uninitialized byte.
Otherwise, it will continue to read into the supplied buffer for the requested number of bytes.

The functions returns the number of bytes read.
--- */
int16_t flashReadData(uint16_t block, uint16_t page, uint16_t index, uint8_t *buffer, uint16_t size, BOOL just_text) {
	if (buffer == NULL)
		return -1;

	buffer[0] = 0;
	int16_t i = 0;
	uint8_t b;

	_flash_wait_ready();
	_spi_enable_slave();
	_spi_tx(READ_DATA);
	_spi_tx_address(block, page, index);
	for (i = 0; i < size; i++) {
		// if we are only interested in text, then DUMMY_DATA means we are done
		b = _spi_tx(DUMMY_DATA);
		if (just_text && (b == DUMMY_DATA))
			break;
		buffer[i] = b;
		if (just_text && (b == 0)) {
			i++;
			break;
		}
	}
	_spi_disable_slave();
	return i;
}


/* ---
int16_t flashReadByte(uint16_t block, uint16_t page, uint16_t index) - 
read a single byte from a page of FLASH storage from the specified block+page+index address.

The Block+Page+Index address indicates a 64 KB block, a 256 Byte Page within that block, and then an index (usually 0) withing that page.

The functions returns the byte.
--- */
uint8_t flashReadByte(uint16_t block, uint16_t page, uint16_t index) {
	uint16_t rtn = DUMMY_DATA;

	_flash_wait_ready();
	_spi_enable_slave();
	_spi_tx(READ_DATA);
	_spi_tx_address(block, page, index);
	rtn = _spi_tx(DUMMY_DATA);
	_spi_disable_slave();

	return rtn;
}

/* ---
BOOL flashFormat() - 
erase  the entire FLASH storage chip and optionally write a marker to indicate it has been *formatted*.

The functions returns TRUE if the erase was successful.
--- */
BOOL flashFormat() {
	// this specific data in the signature is not as important as the fact we can test for it
	_flash_cmd_erase_chip();

	BOOL rtn = (flashReadByte(CHIP_DATA_BLOCK, CHIP_SIGNATURE_PAGE, 0) == DUMMY_DATA);

#ifdef FLASH_FORMAT_MARKER_BYTE
#ifdef FLASH_FORMAT_MARKER_BLOCK
#ifdef FLASH_FORMAT_MARKER_PAGE
	// we mark the flash chip as formatted by storing a marker byte
	_flash_page_buffer[0] = FLASH_FORMAT_MARKER_BYTE;
	flashWriteData(FLASH_FORMAT_MARKER_BLOCK, FLASH_FORMAT_MARKER_PAGE, 0, _flash_page_buffer, 1);
#endif
#endif
#endif
	_flash_formatted = TRUE;	// a format request has been performed (regardless of if it was successful)

	return rtn;
}

/* ---
BOOL flashIsFormated() - 
The functions returns TRUE if the `FLASH_FORMAT_MARKER_BYTE` is detected.

**Note:** if the `FLASH_FORMAT_MARKER_BYTE` was not defined, then this function always returns true.
It is up to the user to perform a chip erase as needed.
--- */
BOOL flashIsFormatted() {
	// test if we ever wrote anything to the marker byte
#ifdef FLASH_FORMAT_MARKER_BYTE
#ifdef FLASH_FORMAT_MARKER_BLOCK
#ifdef FLASH_FORMAT_MARKER_PAGE
	return (flashReadByte(FLASH_FORMAT_MARKER_BLOCK, FLASH_FORMAT_MARKER_PAGE, 0) == FLASH_FORMAT_MARKER_BYTE);
#endif
#endif
#endif
	return (TRUE);
}

/* ---
BOOL flashInited() - returns TRUE if the flashInit() function has already been called and it was successful.
--- */
BOOL flashIsInited() {
	return _flash_inited;
}

/* ---
BOOL flashWasFormatted() - returns TRUE if the FLASH was formatted during the current boot/execution
--- */
BOOL flashWasFormatted() {
	return _flash_formatted;
}


/* ---
void flashInit() - initialization of the FLASH SPI interface and optionally erase and format the FLASH if it is determined to be unformatted.
--- */
void flashInit(void) {
	// SPI init
	SPI_DDR |= (1 << SPI_MOSI_PIN) | (1 << SPI_SCK_PIN);	// set CS, MOSI, and SCLK as outputs
	SPI_DDR &= ~(1 << SPI_MISO_PIN);						// set MISO to input

	FLASH_DDR |= (1 << FLASH_CS_PIN);						// set CS
	FLASH_PORT |= (1 << FLASH_CS_PIN);						// disable CS

	SPCR0 = (1 << SPE) | (1 << MSTR);						// enable SPI, Master, set clock rate fck/4 = 2MHz/4 - 512KHz
	//SPCR0 = (1 << SPE) | (1 << MSTR) | (1 << SPR0);		// enable SPI, Master, set clock rate fck/16 = 2MHz/16 = 125KHz
	//SPSR0 = (1 << SPI2X);									// 2X clock 512 --> 1Mhz

#ifdef FLASH_PWR_PIN
	// set flash power pin as output HIGH
	FLASH_PWR_DDR |= (1 << FLASH_PWR_PIN);
	FLASH_PWR_PORT |= (1 << FLASH_PWR_PIN);
#endif

	_flash_inited = TRUE;
	_flash_formatted = FALSE;

	if (!flashIsFormatted())
		flashFormat();
}

#endif /* FLASH_ENABLED */
