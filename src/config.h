/* ************************************************************************************
* File:    config.h
* Date:    2020.10.04
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
* ************************************************************************************/

/* ---

# config_m4809.h - define all configuration for the specific ECC project using the ATMega4809 microcontroller

All of the ECC subsystems are configured from this one file. It is important to match the definitions
below with the electrical schematic for all used IO Pins.

## Config Notes:

The subsystems use various AVR timers. Those are not defined here but
probably have serious implications if project level code will need to use them. _Sorry._

The original ECCCore was built for the ATMega328PB. The intent was to make it generic and have
all configuration driven the a file like this one. Well, that was a good idea and if this project
had hundreds of users, it would be be warranted. However, it is really only used by Bradán Lane STUDIO
so the decision was to fork the ECCCore with the switch to ATMega4809. _Sorry (again)._

The ATMega4809 Atmel syntax for pins and port operations provides both bit numbers (bp) and bit masks (bm).
This implementation of ECCCore now uses bit masks to simplify code. *Ironically, Atmel is inconsistent
within the `iom4809.h` using HEX mask values in some cases and using bit-shift macros in other cases.*

Example: `PORTA.OUT |= (1 << PIN0_bp);` is the same as `PORTA.OUTSET = PIN0_bm;`

_Well, the best laid plans of mice and men ... and all of that.
It turns out there is need for some of the old microcontroller code so it will get added back, as needed.
The most pressing is the UART support._

--------------------------------------------------------------------------
--- */

/* ---
### CHIP Requirements:
The chip type drives some compiler preprocessing and is used to flag feature incompatibility.
Only one chip type may be defined. The various ISR vectors (interrupt handers) are an example of chip specifc code.
--- */
//#define CHIP_ATMEGA328PB
//#define CHIP_ATMEGA128RFA1
#define CHIP_ATMEGA4809


/* ---
#### PROGMEM_ENABLED
PROGMEM is the ability to store char constants in program space.
--- */
#ifdef CHIP_ATMEGA328PB | CHIP_ATMEGA128RFA1
#define PROGMEM_ENABLED 1
#else
#define PROGMEM_ENABLED 0
#define PROGMEM	// make PROGMEM a non-qualifier for variables and data
#endif

/* ---
### Subsystem Enablement:
Most subsystems are optional and may be included/excluded as appropriate for the specific project.
--- */

/* ---
#### EEPROM_ENABLED
EEPROM is the persistent storage of the microcontroller.
--- */
#define EEPROM_ENABLED	1

/* ---
#### IR_ENABLED
The I/O UART is implemented on UART0. The IR transceiver uses a second UART.
--- */
#define IR_ENABLED	1

/* ---
#### RF_ENABLED
The RF transceiver is a chip specific capability
--- */
#define RF_ENABLED	0

/* ---
#### LEDS_ENABLED
LEDs are charlieplexed and use a timer. Even if LEDs are not enabled, the timer is still used to provide
a clock source for measuring events. Timer TCB0 is used.
--- */
#define LEDS_ENABLED 1

/* ---
#### TOUCH_ENABLED
TOUCH sensors measure the relative capacitance on an IO pin. All sensors are updated in a single synchronous function.
--- */
#define TOUCH_ENABLED 1

/* ---
#### SOUND_ENABLED
The sound subsystem implements a one octave music scale consisting of 12 notes. The sound subsystem
assumes a piezoelectric buzzer on a specific pin to allow for using a hardware timer.
--- */
#define SOUND_ENABLED	1

/* ---
#### BUTTONS_ENABLED
The buttons subsystem provides convenience functions for checking the state of one or more buttons.
--- */
#define BUTTONS_ENABLED	0

/* ---
#### FLASH_ENABLED
storage uses the SPI interface and a CS pin.
--- */
#define FLASH_ENABLED 0

/* ---
#### MPU6050_ENABLED
The MPU6050 uses the I2C interface. The AVR specific pins are used and must have the requisite pull-up resistors.
--- */
#define MPU_ENABLED		0

/* ---
#### LED_ANIMATIONS
There is optional support for animations. It is optional since each animation requires (LED_COUNT+1) 16bit values.

An animation consists of frame duration and a bit mask for each frame.
The duration is the amount of time for one complete cycle of the animation (in milliseconds).

**Hint:** Use the `ecc_animation.xlsx` spreadsheet to calculate the LED masks!
--- */

#define LED_ANIMATIONS 1


/* ---
#### SLEEP_MESSAGE_ENABLED
It can be handy to display a message when entering sleep mode and when exiting sleep mode.
--- */
#define SLEEP_MESSAGE_ENABLED 1
#if SLEEP_MESSAGE_ENABLED
#define SLEEP_MESSAGE "sleeping now"
#define WAKE_MESSAGE  "waking up"
#endif

/* ---
### Fixed Purpose Pins:
Very few pins of the ATMega4809 have explicit meaning but several have default capabilities
and are used within the ecccore. It is important to uses these pins appropriately within
the project schematic. They are included here to make it clear not to use them
for other purposes.
--- */

/* ---
The UPDI pin must not be used.
--- */

/* ---
The USARTs on the ATMega4809 uses PIN0 and PIN1 on PORTA, PORTC, PORTF, and PORTB corresponding to USART0 .. USART3.
Each USART also supports an alternate pin assignment using PIN4 and PIN5 on the same ports.
The code in `uart.h` assume the primary pin assignments are used.

--- */
#define UART_BAUD 			9600
#define UART 				USART0
#define UART_PORT			PORTA
#define TX_PIN				PIN0_bm
#define RX_PIN				PIN1_bm
#define UART_TX_INTERRUPT	USART0_DRE_vect
#define UART_RX_INTERRUPT	USART0_RXC_vect

#if IR_ENABLED
#define IR_BAUD 			1200
#define IR 					USART2
#define IR_PORT				PORTF
#define IR_TX_PIN			PIN0_bm
#define IR_RX_PIN			PIN1_bm
#define IR_TX_INTERRUPT		USART2_DRE_vect
#define IR_RX_INTERRUPT		USART2_RXC_vect
#endif

/* ---
The wake from sleep uses ???? (pin PA6)
--- */
#define WAKE_PORT			PORTA
#define WAKE_PIN			PIN6_bm
#define WAKE_CTRL			PIN6CTRL

/* ---
The sound subsystem uses hardware timer
--- */
#if SOUND_ENABLED
#define BUZZER_PORT			PORTC
#define BUZZER_PIN			PIN0_bm
#endif

/* ---
The MPU6050 uses an I2C interface on I2C (pin PA2 and PA3); there are alternate pins for I2C (pin PC2 and PC3)
--- */
#if MPU_ENABLED
#define I2C_PORT			PORTA
#define I2C_SDA_PIN			PIN2_bm
#define I2C_SCL_PIN			PIN3_bm
#endif

/* ---
**Synopsis:** At this point, we have used ???? pins
--- */

// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------

/* ---
### User Define Power Output Pins:
To conserve power during sleep, all attached peripherals must also be powered down.
A simple solution is to power them from GPIO pins. This assumes the peripheral power
requirements do not exceed the output of an GPIO pin.

If a peripheral is only needed for a short time, vs anytime the device is awake, then
that peripheral should have it's own pin.
--- */

/* ---
The touch sensors use power to charge the TOUCH during the measurement cycle. All touch sensors use a single power source.
--- */
#define TOUCH_PWR_PORT		PORTE
#define TOUCH_PWR_PIN		PIN3_bm			// feed power to the touch sensors

/* ---
The MPU6050 requires power as does the I2C pull up resistors
--- */
#if MPU_ENABLED
#define I2C_PWR_PORT		TOUCH_PWR_PORT
#define I2C_PWR_PIN			TOUCH_PWR_PIN			// feed power to the I2C device and resistors
#endif

/* ---
The FLASH storage chip requires power.
--- */
#if FLASH_AVAILABLE
#define FLASH_PWR_PORT		TOUCH_PWR_PORT
#define FLASH_PWR_PIN		TOUCH_PWR_PIN			// feed power to FLASH chip
#endif


// ---------------------------------------------------------------------------------------------------------------------



// ---------------------------------------------------------------------------------------------------------------------

/* ---
### User Defined IO Pins:
Several general IO pins are needed within the core. It is important to match the definitions with the project schematic.
--- */

/* ---
The IR circuit forms a hardware level UART. The photo-transistor requires power for its pull up.
--- */
#if IR_ENABLED
#define IR_PWR_PORT			PORTF
#define IR_PWR_PIN			PIN3_bm	// feed power to the photo-transistor
#endif

/* ---
While SPI is defined by the AVR, when SPI is shared with other devices, those devices each require a CS pin.
--- */
#if FLASH_AVAILABLE
#define FLASH_PORT			PORTB
#define FLASH_CS_PIN		PIN2_bm
#endif

/* ---
An ECC project may have buttons or interactions connected to pins.
Each button requires a port and a pin assignment. Currently all buttons are assumed pressed when LOW.
--- */

#if BUTTONS_ENABLED
#define BUTTON_COUNT 1
PORT_t* _button_ports[BUTTON_COUNT] = { &PORTA };
uint8_t _button_pins[BUTTON_COUNT] = { PIN7_bm };
uint8_t* _button_ctrls[BUTTON_COUNT] = { &(PORTA.PIN7CTRL) };

// optionally give names to buttons to improve code readability; these names are not used in teh sybsystem
#define BUTTON_1 0
#endif

// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------

/* ---
### LED IO Pins:
The LEDs are charlieplexed to reduce power usage and pin usage. The code assumed all LED pins are within a single IO PORT.
Charlieplexed LEDs require 2 pins for 2 LEDs; 3 pins for 6 LEDs; 4 pins for 12 LEDs; 5 pins for 20 LEDs; and 6 pins for 30 LEDs.
--- */

/* ---
There are many different ways to define the LEDs. The minimum definitions and declarations
include: `LEDS_COUNT`, `ALL_LEDS`, `LED_DDR`, `LED_PORT`, and the `_leds_pins[]`, `_leds_out[]`, & `_leds_order[]` arrays.
--- */

#define LEDS_COUNT		10

#define LED_PORT 		PORTC

#define CP1_PIN 		PIN4_bm
#define CP2_PIN 		PIN5_bm
#define CP3_PIN 		PIN6_bm
#define CP4_PIN 		PIN7_bm

#define ALL_LEDS ((CP1_PIN) | (CP2_PIN) | (CP3_PIN) | (CP4_PIN))

/* ---
**Hint:** If you find you are mixing up the order of the LEDs because of the bidirectional nature of the pins and charlieplexing
then you may find it helpful to define each LED's pins and direction and then use those to initialize the data arrays.
--- */


#define LED1_PINS	((CP1_PIN) | (CP2_PIN))
#define LED1_OUT	(CP2_PIN)
#define LED2_PINS	((CP1_PIN) | (CP2_PIN))
#define LED2_OUT	(CP1_PIN)

#define LED3_PINS	((CP1_PIN) | (CP3_PIN))
#define LED3_OUT	(CP3_PIN)
#define LED4_PINS	((CP1_PIN) | (CP3_PIN))
#define LED4_OUT	(CP1_PIN)

#define LED5_PINS	((CP1_PIN) | (CP4_PIN))
#define LED5_OUT	(CP4_PIN)
#define LED6_PINS	((CP1_PIN) | (CP4_PIN))
#define LED6_OUT	(CP1_PIN)

#define LED7_PINS	((CP2_PIN) | (CP3_PIN))
#define LED7_OUT	(CP3_PIN)
#define LED8_PINS	((CP2_PIN) | (CP3_PIN))
#define LED8_OUT	(CP2_PIN)

#define LED9_PINS	((CP2_PIN) | (CP4_PIN))
#define LED9_OUT	(CP4_PIN)
#define LED0_PINS	((CP2_PIN) | (CP4_PIN))
#define LED0_OUT	(CP2_PIN)

//DDRB direction config for each LED (1 = output)
//PORTB output config for each LED (1 = High aka powered, 0 = Low aka ground)
const uint8_t _leds_pins[LEDS_COUNT] = {	LED1_PINS,	LED2_PINS,	LED3_PINS,	LED4_PINS,	LED5_PINS,	LED6_PINS,	LED7_PINS,	LED8_PINS,	LED9_PINS,	LED0_PINS,};
const uint8_t _leds_out[LEDS_COUNT] = {	LED1_OUT,	LED2_OUT,	LED3_OUT,	LED4_OUT,	LED5_OUT,	LED6_OUT,	LED7_OUT,	LED8_OUT,	LED9_OUT,	LED0_OUT,};

// we want to sequence (only used by the sequencer) the LEDs in the following order: (zero-based)
const uint8_t _leds_order[LEDS_COUNT] = {8, 6, 5, 3, 2, 0, 9, 7, 4, 1};


// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------

/* ---
### Touch Sensor IO Pins:
Touch sensors require a specific project schematic and PCB design.

The touch sensing is performed synchronously.

With only a single PORT, the maximum number of touch sensors is 8, but this only is possible when using PORT B and not using any conflicting AVR capabilities.
With PORT C or D, the maximum number of touch sensors is 6.

The code had been expanded - *and made a bit more complex* - to handle touch sensors across two AVR PORTS. It would be smaller code and
slightly more efficient if all sensors were within a single AVR PORT. When `TOUCH_PORTS_2X` is ZERO, the code will optimize.
--- */

#define TOUCH_PORTS_2X 1

/* ---
There are many different ways to define the Touch sensors. The minimum definitions and declarations include:
- `TOUCH_PORT_n?`
- `TOUCHn_PIN`
- `TOUCH_COUNT`
- `TOUCH_SPLIT` (when two ports are used)

The calibration of the touch sensors is configured using either:
`TOUCH_THRESHOLD`
`TOUCH_READINGS[]` (array of touch readings used for individual sensor calibration)

If all touch sensors have the same raw reading, then the `TOUCH_THRESHOLD` may be used.
Alternately, if a test of the touch sensors - when no contact is made - shows different values for each then
the `TOUCH_READINGS[]` array may be set with those readings. The code then uses the readings as abaseline and
applies `TOUCH_DELTA` to each to determine touch activation. _(The touch readings are reported by the demo code in `main()`)_

**NOTE1:** It is rare that `TOUCH_MEASUREMENT_ITERATIONS` or `TOUCH_UPPER_LIMIT` need to be changed but there may
be cases where the touch range needs greater latitude.

**NOTE1:** The threshold and readings are based on the time it takes the code to test and loop.
For this reason, the values are affected by the MCU clock speed `F_CPU`. The initial values provided  here are based on tests
with a MCU running at 2MHz.

--- */

#define TOUCH_PORT_0 PORTE

#if TOUCH_PORTS_2X
#define TOUCH_PORT_1 PORTD
#endif

#define TOUCH1_PIN	PIN0_bm
#define TOUCH2_PIN	PIN1_bm
#define TOUCH3_PIN	PIN2_bm

#define TOUCH4_PIN	PIN7_bm
#define TOUCH5_PIN 	PIN6_bm
#define TOUCH6_PIN 	PIN5_bm
#define TOUCH7_PIN 	PIN4_bm
#define TOUCH8_PIN 	PIN3_bm
#define TOUCH9_PIN 	PIN2_bm
#define TOUCH0_PIN	PIN1_bm

#define TOUCH_COUNT 10
#if TOUCH_PORTS_2X
#define TOUCH_SPLIT 3		// if sensors span 2 ports, this defines the index of the switch point
#endif

uint8_t _touch_pins[TOUCH_COUNT] = {TOUCH1_PIN, TOUCH2_PIN, TOUCH3_PIN, TOUCH4_PIN, TOUCH5_PIN, TOUCH6_PIN, TOUCH7_PIN, TOUCH8_PIN, TOUCH9_PIN, TOUCH0_PIN};

#define TOUCH_MEASUREMENT_ITERATIONS	3	// number of samples taken and then averaged together to give a usable measurement
#define TOUCH_UPPER_LIMIT				64	// needs to greater than threshold and less that 256; set to 256 to disable; higher values would allow for variable touch data
#define TOUCH_DEFAULT_CALIBRATION		16
//#define TOUCH_THRESHOLD 				13	// best if this is a multiple of iterations +1

#ifndef TOUCH_THRESHOLD
// allow for touch sensor specific thresholds - this may occur is the board has non-uniform capacitance characteristics
// WARNING: only use this after testing a sampling of PCBs to make sure it is the PCB design and not just individual PCBs
// NOTE: this capability would allow for self calibrating touch sensors
#define TOUCH_DELTA (TOUCH_MEASUREMENT_ITERATIONS+2)
// we define 'readings' since its easier to calibrate
// we start with all limits set high and then autocalibrate on first start
#endif

// add defines to make code more readable
#define TOUCH_PAD_1 0
#define TOUCH_PAD_2 1
#define TOUCH_PAD_3 2
#define TOUCH_PAD_4 3
#define TOUCH_PAD_5 4
#define TOUCH_PAD_6 5
#define TOUCH_PAD_7 6
#define TOUCH_PAD_8 7
#define TOUCH_PAD_9 8
#define TOUCH_PAD_0 9


/* ---
**Tip:** It is not used by the core code but it is helpful for project development to create a series of bit masks for combinations of
touch sensors which will be used to have specific meanings. The most common is `TOUCH_ALL` but many other combinations could be created.
By creating bit maps of the combinations, test cases are very efficient.
--- */

#define TOUCH_ALL_BITS		0b0000001111111111	// all sensors
#define TOUCH_LEFT_BITS		0b0000000001001001
#define TOUCH_RIGHT_BITS	0b0000000100100100
#define TOUCH_CORNER_BITS	0b0000000101000101

// map touch array index to physical positions; improves code readability
#define TOUCH_LEFT_TOP		(TOUCH_PAD_1)
#define TOUCH_LEFT_MIDDLE	(TOUCH_PAD_4)
#define TOUCH_LEFT_BOTTOM	(TOUCH_PAD_7)
#define TOUCH_RIGHT_TOP		(TOUCH_PAD_3)
#define TOUCH_RIGHT_MIDDLE	(TOUCH_PAD_6)
#define TOUCH_RIGHT_BOTTOM	(TOUCH_PAD_9)
#define TOUCH_BONUS1		(TOUCH_PAD_2)
#define TOUCH_BONUS2		(TOUCH_PAD_5)
#define TOUCH_BONUS3		(TOUCH_PAD_8)
#define TOUCH_BONUSX		(TOUCH_PAD_0)

// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------

/* ---
### EEPROM Setup:
No specific configuration setup is *required* for the EEPROM. However, for `format` detection to work, a user defined bytecode
and location is required. These are optional definitions and the code will optimize away format detection if these are not defined.

If the `FORMAT_MARKER` definitions are present, the eeprom *format* related functions are useful for storing initial values in EEPROM.

*Note:* The ATMega4809 has only 256 bytes of EEPROM and the functions take use a relative address space starting at 0x0000
--- */

#if EEPROM_ENABLED
#define EEPROM_FORMAT_MARKER_BYTE		0xAA	// any byte code other than 0x00 and 0xFF
#endif

// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------

/* ---
### FLASH Setup:
No specific configuration setup is *required* for the FLASH. However, for `format` detection to work, a user defined bytecode
and location is required. These are optional definitions and the code will optimize away format detection if these are not defined.

If the `FORMAT_MARKER` definitions are present, the eeprom *format* related functions are useful for storing initial values in FLASH.
--- */

#if FLASH_ENABLED
#define FLASH_FORMAT_MARKER_BYTE		0xAA	// any byte code other than 0x00 and 0xFF
#define FLASH_FORMAT_MARKER_BLOCK		0x0000	// any block less than the MAX/CHIP_BLOCK_SIZE
#define FLASH_FORMAT_MARKER_PAGE		0x0000	// any page less than the CHIP_BLOCK_SIZE/CHIP_PAGE_SIZE
#endif

// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------

/* ---
### Power Idle Timeouts:
The power-down sleep capability can act on idle detection. There are twe functions to quickly indicate interaction and reset the idle timeout.
The `NORMAL_SLEEP_DELAY` and `LONG_SLEEP_DELAY` definitions are used to control defaults for idle timeout.
--- */
#define NORMAL_SLEEP_DELAY		 60000	// 60 seconds
#define LONG_SLEEP_DELAY		300000	// 5 minutes
#define WARNING_SLEEP_DELAY		 10000	// 10 seconds
#define POWER_RELEASE_DELAY		   150	// very short for physical power button; longer if the using a pair of contact pads
/* ---
### SLEEP_MESSAGE Setup:
The sleep / wake / WARNING messages are not required. If any of the the `SLEEP_MESSAGE`, `WAKE_MESSAGE`, or  `WARNING_MESSAGE` definitions and not set, the corresponding code will be optimized away.
--- */

#if SLEEP_MESSAGE_ENABLED
#define WARNING_MESSAGE	"sleep in 10 seconds"
#define SLEEP_MESSAGE 	"sleeping now"
#define WAKE_MESSAGE  	"waking up"
#endif

// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------

/* ---
### GAME_MESSAGES:
Optional IR start/finish messages to UART and optional extra bytes for the IR data packet (useful for differentiating projects)
--- */

// Chicago Titles
#define GAME_BEGIN_MESSAGE	"Game Begin"
#define GAME_WIN_MESSAGE	"You Won"
#define GAME_LOSE_MESSAGE	"You Lost"

// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------

/* ---
### IR MESSAGES:
Optional IR start/finish messages to UART and optional extra bytes for the IR data packet (useful for differentiating projects)
--- */

// Chicago Titles
#define IR_STARTUP_MESSAGE	"IR Start"
#define IR_SHUTDOWN_MESSAGE	"IR Stop"
#define IR_MSG_EXTRAS "\d19\d69"	// Chicago First Studio Album (actual Chicago Transit Authority)

// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------

/* ---
### I/O Buffers:
**Assumptions:** The recipient of transmitted data is fast enough that outbound data rarely blocks on queuing data into the TX buffer.
The receive buffer often needs to be larger to accommodate incoming data when other ECC activity delays pulling data off the RX buffer.
When also using the IR UART, it is not necessary they have the same buffer sizes if their purpose if significantly different.
When also using the RF, the TX buffer is limited by the size of the transceiver FRAME with is 127+1 bytes.

--- */

// Number of bytes for UART buffers.
#define UART_TX_BUFFER_SIZE 0x080
#define UART_RX_BUFFER_SIZE 0x100
#if IR_ENABLED
#define IR_TX_BUFFER_SIZE 0x040
#define IR_RX_BUFFER_SIZE 0x040
#endif

#if RF_ENABLED
#define RF_TX_BUFFER_SIZE 0x080	// no need for it to be greater than 128 bytes since RF uses a FRAME with a max of 127+1 bytes
#define RF_RX_BUFFER_SIZE 0x100
#endif


// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------

// chip specifc validation
#ifndef CHIP_ATMEGA4809
#if IR_ENABLED
#error IR support is only available with the ATMega4809 chip
#endif
#if TOUCH_ENABLED
#error TOUCH support is only available with the ATMega4809 chip
#endif
#if SOUND_ENABLED
#error SOUND support is only available with the ATMega4809 chip
#endif
#if LEDS_ENABLED
#error LED support is only available with the ATMega4809 chip
#endif
#endif

// chip specifc validation
#ifndef CHIP_ATMEGA128RFA1
#if RF_ENABLED
#error RF support is only available with the ATMega128RFA1 chip
#endif
#endif

// ---------------------------------------------------------------------------------------------------------------------

