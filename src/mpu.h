/* ************************************************************************************
* File:    mpu.h
* Date:    2020.05.04
* Author:  Bradan Lane Studio
*
* based on code from: https://github.com/YifanJiangPolyU/MPU6050
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## mpu.h - read gyroscope and acceleration data from an MPU6050 or MPU9250 I2C device

This module provides basic access to read the data for the 3-axis gyro + 3-axis accelerometer of the IMU.

**Disclaimer:** This code is really gorpy and is derived from the work of Yifan Jiang.

--------------------------------------------------------------------------
--- */



#if !MPU_ENABLED

#define mpuReadGyroX(buff)
#define mpuReadGyroY(buff)
#define mpuReadGyroZ(buff)
#define mpuReadGyro(buff)
#define mpuReadAccelX(buff)
#define mpuReadAccelY(buff)
#define mpuReadAccelZ(buff)
#define mpuReadAccel(buff)
#define mpuReadOrientation(buff)
#define mpuInit()
#define mpuIsInited() {FALSE}

#else

/*
	define mpu6050 register addresses here
*/

#define MPU6050_ADDRESS 0x68
#define MPU9250_ADDRESS 0x71

#define MPU6050_RA_XG_OFFS_TC 0x00 //[7] PWR_MODE, [6:1] XG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_RA_YG_OFFS_TC 0x01 //[7] PWR_MODE, [6:1] YG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_RA_ZG_OFFS_TC 0x02 //[7] PWR_MODE, [6:1] ZG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_RA_X_FINE_GAIN 0x03 //[7:0] X_FINE_GAIN
#define MPU6050_RA_Y_FINE_GAIN 0x04 //[7:0] Y_FINE_GAIN
#define MPU6050_RA_Z_FINE_GAIN 0x05 //[7:0] Z_FINE_GAIN
#define MPU6050_RA_XA_OFFS_H 0x06 //[15:0] XA_OFFS
#define MPU6050_RA_XA_OFFS_L_TC 0x07
#define MPU6050_RA_YA_OFFS_H 0x08 //[15:0] YA_OFFS
#define MPU6050_RA_YA_OFFS_L_TC 0x09
#define MPU6050_RA_ZA_OFFS_H 0x0A //[15:0] ZA_OFFS
#define MPU6050_RA_ZA_OFFS_L_TC 0x0B
#define MPU6050_RA_XG_OFFS_USRH 0x13 //[15:0] XG_OFFS_USR
#define MPU6050_RA_XG_OFFS_USRL 0x14
#define MPU6050_RA_YG_OFFS_USRH 0x15 //[15:0] YG_OFFS_USR
#define MPU6050_RA_YG_OFFS_USRL 0x16
#define MPU6050_RA_ZG_OFFS_USRH 0x17 //[15:0] ZG_OFFS_USR
#define MPU6050_RA_ZG_OFFS_USRL 0x18
#define MPU6050_RA_SMPLRT_DIV 0x19
#define MPU6050_RA_CONFIG 0x1A
#define MPU6050_RA_GYRO_CONFIG 0x1B
#define MPU6050_RA_ACCEL_CONFIG 0x1C
#define MPU6050_RA_FF_THR 0x1D
#define MPU6050_RA_FF_DUR 0x1E
#define MPU6050_RA_MOT_THR 0x1F
#define MPU6050_RA_MOT_DUR 0x20
#define MPU6050_RA_ZRMOT_THR 0x21
#define MPU6050_RA_ZRMOT_DUR 0x22
#define MPU6050_RA_FIFO_EN 0x23
#define MPU6050_RA_I2C_MST_CTRL 0x24
#define MPU6050_RA_I2C_SLV0_ADDR 0x25
#define MPU6050_RA_I2C_SLV0_REG 0x26
#define MPU6050_RA_I2C_SLV0_CTRL 0x27
#define MPU6050_RA_I2C_SLV1_ADDR 0x28
#define MPU6050_RA_I2C_SLV1_REG 0x29
#define MPU6050_RA_I2C_SLV1_CTRL 0x2A
#define MPU6050_RA_I2C_SLV2_ADDR 0x2B
#define MPU6050_RA_I2C_SLV2_REG 0x2C
#define MPU6050_RA_I2C_SLV2_CTRL 0x2D
#define MPU6050_RA_I2C_SLV3_ADDR 0x2E
#define MPU6050_RA_I2C_SLV3_REG 0x2F
#define MPU6050_RA_I2C_SLV3_CTRL 0x30
#define MPU6050_RA_I2C_SLV4_ADDR 0x31
#define MPU6050_RA_I2C_SLV4_REG 0x32
#define MPU6050_RA_I2C_SLV4_DO 0x33
#define MPU6050_RA_I2C_SLV4_CTRL 0x34
#define MPU6050_RA_I2C_SLV4_DI 0x35
#define MPU6050_RA_I2C_MST_STATUS 0x36
#define MPU6050_RA_INT_PIN_CFG 0x37
#define MPU6050_RA_INT_ENABLE 0x38
#define MPU6050_RA_DMP_INT_STATUS 0x39
#define MPU6050_RA_INT_STATUS 0x3A
#define MPU6050_RA_ACCEL_XOUT_H 0x3B
#define MPU6050_RA_ACCEL_XOUT_L 0x3C
#define MPU6050_RA_ACCEL_YOUT_H 0x3D
#define MPU6050_RA_ACCEL_YOUT_L 0x3E
#define MPU6050_RA_ACCEL_ZOUT_H 0x3F
#define MPU6050_RA_ACCEL_ZOUT_L 0x40
#define MPU6050_RA_TEMP_OUT_H 0x41
#define MPU6050_RA_TEMP_OUT_L 0x42
#define MPU6050_RA_GYRO_XOUT_H 0x43
#define MPU6050_RA_GYRO_XOUT_L 0x44
#define MPU6050_RA_GYRO_YOUT_H 0x45
#define MPU6050_RA_GYRO_YOUT_L 0x46
#define MPU6050_RA_GYRO_ZOUT_H 0x47
#define MPU6050_RA_GYRO_ZOUT_L 0x48
#define MPU6050_RA_EXT_SENS_DATA_00 0x49
#define MPU6050_RA_EXT_SENS_DATA_01 0x4A
#define MPU6050_RA_EXT_SENS_DATA_02 0x4B
#define MPU6050_RA_EXT_SENS_DATA_03 0x4C
#define MPU6050_RA_EXT_SENS_DATA_04 0x4D
#define MPU6050_RA_EXT_SENS_DATA_05 0x4E
#define MPU6050_RA_EXT_SENS_DATA_06 0x4F
#define MPU6050_RA_EXT_SENS_DATA_07 0x50
#define MPU6050_RA_EXT_SENS_DATA_08 0x51
#define MPU6050_RA_EXT_SENS_DATA_09 0x52
#define MPU6050_RA_EXT_SENS_DATA_10 0x53
#define MPU6050_RA_EXT_SENS_DATA_11 0x54
#define MPU6050_RA_EXT_SENS_DATA_12 0x55
#define MPU6050_RA_EXT_SENS_DATA_13 0x56
#define MPU6050_RA_EXT_SENS_DATA_14 0x57
#define MPU6050_RA_EXT_SENS_DATA_15 0x58
#define MPU6050_RA_EXT_SENS_DATA_16 0x59
#define MPU6050_RA_EXT_SENS_DATA_17 0x5A
#define MPU6050_RA_EXT_SENS_DATA_18 0x5B
#define MPU6050_RA_EXT_SENS_DATA_19 0x5C
#define MPU6050_RA_EXT_SENS_DATA_20 0x5D
#define MPU6050_RA_EXT_SENS_DATA_21 0x5E
#define MPU6050_RA_EXT_SENS_DATA_22 0x5F
#define MPU6050_RA_EXT_SENS_DATA_23 0x60
#define MPU6050_RA_MOT_DETECT_STATUS 0x61
#define MPU6050_RA_I2C_SLV0_DO 0x63
#define MPU6050_RA_I2C_SLV1_DO 0x64
#define MPU6050_RA_I2C_SLV2_DO 0x65
#define MPU6050_RA_I2C_SLV3_DO 0x66
#define MPU6050_RA_I2C_MST_DELAY_CTRL 0x67
#define MPU6050_RA_SIGNAL_PATH_RESET 0x68
#define MPU6050_RA_MOT_DETECT_CTRL 0x69
#define MPU6050_RA_USER_CTRL 0x6A
#define MPU6050_RA_PWR_MGMT_1 0x6B
#define MPU6050_RA_PWR_MGMT_2 0x6C
#define MPU6050_RA_BANK_SEL 0x6D
#define MPU6050_RA_MEM_START_ADDR 0x6E
#define MPU6050_RA_MEM_R_W 0x6F
#define MPU6050_RA_DMP_CFG_1 0x70
#define MPU6050_RA_DMP_CFG_2 0x71
#define MPU6050_RA_FIFO_COUNTH 0x72
#define MPU6050_RA_FIFO_COUNTL 0x73
#define MPU6050_RA_FIFO_R_W 0x74
#define MPU6050_RA_WHO_AM_I 0x75

#define MPU6050_PWR_RESET	(1<<7)
#define MPU6050_PWR_SLEEP	(1<<6)
#define MPU6050_PWR_TMPOFF	(1<<3)



/*
	MPU 6050 code starts here
*/


#include <compat/twi.h>
#include <inttypes.h>
#include <stdint.h>
#include <math.h>


#define I2C_READ 1  // defines the data direction (reading from I2C device) in i2c_start(),i2c_rep_start()
#define I2C_WRITE 0 // defines the data direction (writing to I2C device) in i2c_start(),i2c_rep_start()

/* I2C clock in Hz */
#define SCL_CLOCK 100000L

// allow switching between m328p and m328pb
#ifndef TWBR
#define TWBR TWBR1				   //_SFR_MEM8(0xB8)
#define TWSR TWSR1				   //_SFR_MEM8(0xB9)
#define TWAR TWAR1				   //_SFR_MEM8(0xBA)
#define TWDR TWDR1				   //_SFR_MEM8(0xBB)
#define TWCR TWCR1				   //_SFR_MEM8(0xBC)
#define TWAMR TWAMR1			   //_SFR_MEM8(0xBD)
#define TWI_vect_num TWI1_vect_num //24
#define TWI_vect TWI1_vect		   //_VECTOR(24)
#define PRR PRR1
#define PRTWI PRTWI1

#endif

#define TWCR_DEFAULT ((1 << TWEA) | (1 << TWEN) | (1 << TWIE))
#define TWCR_ACK (TWCR_DEFAULT | (1 << TWINT))
#define TWCR_NOT_ACK (TWCR_ACK & ~(1 << TWEA))

#define TWCR_START (TWCR_DEFAULT | (1 << TWINT) | (1 << TWSTA))
#define TWCR_STOP (TWCR_DEFAULT | (1 << TWINT) | (1 << TWSTO))

BOOL _mpu_inited; // keep track of the existence of the MPU chip on the I2C buss
BOOL _i2c_inited; // keep track of the existence of the MPU chip on the I2C buss

/*************************************************************************
 Initialization of the I2C bus interface. Need to be called only once
*************************************************************************/
void i2c_init(void) {
	if (_i2c_inited)
		return;

	//uint8_t sreg;
	//sreg = SREG;
	//cli();		// disable interrupts

	PRR &= ~(1 << PRTWI); // insure the power reduction for TWI is off

	/* initialize TWI clock: 100 kHz clock, TWPS = 0 => prescaler = 1 */
	TWSR = 0;							   // no prescaler
	TWBR = ((F_CPU / SCL_CLOCK) - 16) / 2; // must(?) be > 10 for stable operation; the result is 2 for a CPU at 2Mhz

	I2C_PORT |= ((1 << I2C_SDA_PIN) | (1 << I2C_SCL_PIN)); // pull-up SDA and SCL pins to activate
	TWCR = (1 << TWEN) | (1 << TWIE) | (1 << TWEA);
	TWAR = 0; // disable slave mode ?

	//SREG = sreg;	// restore interrupts
	_i2c_inited = true;
} /* i2c_init */

/*************************************************************************
  Issues a start condition and sends address and transfer direction.
  return 0 = device accessible, 1= failed to access device
*************************************************************************/
unsigned char i2c_start(uint8_t address) {
	if (!_i2c_inited)
		return 1;

	uint8_t twst;

	// send START condition
	TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN); // to request and acknowledge, then or (1<< TWEA) to TWCR0

	// wait until transmission completed
	while (!(TWCR & (1 << TWINT)))
		;

	// check value of TWI Status Register. Mask prescaler bits.
	twst = TWSR & 0xF8;
	if ((twst != TW_START) && (twst != TW_REP_START))
		return 1;

	// send device address
	TWDR = address;
	TWCR = (1 << TWINT) | (1 << TWEN);

	// wail until transmission completed and ACK/NACK has been received
	while (!(TWCR & (1 << TWINT)))
		;

	// check value of TWI Status Register. Mask prescaler bits.
	twst = TWSR & 0xF8;
	if ((twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK))
		return 1;

	return 0;

} /* i2c_start */

/*************************************************************************
 Issues a start condition and sends address and transfer direction.
 If device is busy, use ack polling to wait until device is ready

 Input:   address and transfer direction of I2C device
*************************************************************************/
void i2c_start_wait(uint8_t address) {
	uint8_t twst;

	twst = i2c_start(address);
	if (twst) {
		while (1) {
			// send START condition
			TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTA);

			// wait until transmission completed
			while (!(TWCR & (1 << TWINT)))
				;

			// check value of TWI Status Register. Mask prescaler bits.
			twst = TWSR & 0xF8;
			if ((twst != TW_START) && (twst != TW_REP_START))
				continue;

			// send device address
			TWDR = address;
			TWCR = (1 << TWINT) | (1 << TWEN);

			// wail until transmission completed
			while (!(TWCR & (1 << TWINT)))
				;

			// check value of TWI Status Register. Mask prescaler bits.
			twst = TWSR & 0xF8;
			if ((twst == TW_MT_SLA_NACK) || (twst == TW_MR_DATA_NACK)) {
				/* device busy, send stop condition to terminate write operation */
				TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);

				// wait until stop condition is executed and bus released
				while (TWCR & (1 << TWSTO))
					;

				continue;
			}
			//if( twst != TW_MT_SLA_ACK) return 1;
			break;
		}
	}
} /* i2c_start_wait */

/*************************************************************************
 Issues a repeated start condition and sends address and transfer direction

 Input:   address and transfer direction of I2C device

 Return:  0 device accessible
          1 failed to access device
*************************************************************************/
unsigned char i2c_rep_start(uint8_t address) {
	return i2c_start(address);

} /* i2c_rep_start */

/*************************************************************************
 Terminates the data transfer and releases the I2C bus
*************************************************************************/
void i2c_stop(void) {
	/* send stop condition */
	TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);

	// wait until stop condition is executed and bus released
	while (TWCR & (1 << TWSTO))
		;
	//_i2c_inited = false;
} /* i2c_stop */

// count the number of I2C devices detected
uint8_t i2c_scan() {
	if (!_i2c_inited)
		return 0;

	int i;
	uint8_t count = 0;

	for (i = 1; i < 127; i++) {
		if (i2c_start((i << 1) | I2C_WRITE)) {
			// didn't work
			//uartPutNumber(i, "no........\n");
		} else {
			uartPutString("yes ");
			uartPutNumber(i);
			uartPutStringNL("");
			count++;
		}
	}
	uartPutString("found ");
	uartPutNumber(count);
	uartPutStringNL("");

	return count;
}

/*************************************************************************
  Send one byte to I2C device

  Input:    byte to be transfered
  Return:   0 write successful
            1 write failed
*************************************************************************/
unsigned char i2c_write(unsigned char data) {
	if (!_i2c_inited)
		return 1;

	uint8_t twst;

	// send data to the previously addressed device
	TWDR = data;
	TWCR = (1 << TWINT) | (1 << TWEN);

	// wait until transmission completed
	while (!(TWCR & (1 << TWINT)))
		;

	// check value of TWI Status Register. Mask prescaler bits
	twst = TWSR & 0xF8;
	if (twst != TW_MT_DATA_ACK)
		return 1;
	return 0;

} /* i2c_write */

/*************************************************************************
 Read one byte from the I2C device, request more data from device

 Return:  byte read from I2C device
*************************************************************************/
unsigned char i2c_readAck(void) {
	if (!_i2c_inited)
		return 0;

	TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA);
	while (!(TWCR & (1 << TWINT)))
		;

	return TWDR;
} /* i2c_readAck */

/*************************************************************************
 Read one byte from the I2C device, read is followed by a stop condition

 Return:  byte read from I2C device
*************************************************************************/
unsigned char i2c_readNak(void) {
	if (!_i2c_inited)
		return 0;

	TWCR = (1 << TWINT) | (1 << TWEN);
	while (!(TWCR & (1 << TWINT)))
		;

	return TWDR;

} /* i2c_readNak */

// read one byte from dev, stored in value, return 1 for error
void i2c_read_byte(uint8_t dev_addr, uint8_t reg_addr, uint8_t *data) {
	*data = 0;
	if (!_i2c_inited)
		return;

	i2c_start_wait((dev_addr << 1) | I2C_WRITE);
	//	_delay_ms(1);
	i2c_write(reg_addr);					   //write address of register to read
											   //	_delay_ms(1);
	i2c_rep_start((dev_addr << 1) | I2C_READ); //restart i2c to start reading
											   //	_delay_ms(1);
	*data = i2c_readNak();
	//	_delay_ms(1);
	i2c_stop();
	//	_delay_ms(1);
}

// write one byte to dev
void i2c_write_byte(uint8_t dev_addr, uint8_t reg_addr, uint8_t data) {
	if (!_i2c_inited)
		return;

	i2c_start_wait((dev_addr << 1) | I2C_WRITE);
	//	_delay_ms(1);
	i2c_write(reg_addr);
	//	_delay_ms(1);
	i2c_write(data);
	//	_delay_ms(1);
	i2c_stop();
	//	_delay_ms(1);
}

// read multiple bytes from dev
void i2c_read_bytes(uint8_t dev_addr, uint8_t first_reg_addr, uint8_t length, uint8_t *data) {
	uint8_t i;
	if (!_i2c_inited)
		return;

	i2c_start_wait((dev_addr << 1) | I2C_WRITE); //start i2c to write register address
												 //	_delay_ms(1);
	i2c_write(first_reg_addr);					 //write address of register to read
												 //	_delay_ms(1);
	i2c_rep_start((dev_addr << 1) | I2C_READ);   //restart i2c to start reading
												 //	_delay_ms(1);

	for (i = 0; i < length - 1; i++) {
		*(data + i) = i2c_readAck();
		//	_delay_ms(1);
	}
	*(data + i) = i2c_readNak();
	//	_delay_ms(1);
	i2c_stop();
	//	_delay_ms(1);
}

// write multiple bytes to dev
void i2c_write_bytes(uint8_t dev_addr, uint8_t reg_addr, uint8_t length, uint8_t *data) {
	uint8_t i;
	if (!_i2c_inited)
		return;

	i2c_start_wait((dev_addr << 1) | I2C_WRITE);
	//	_delay_ms(1);
	i2c_write(reg_addr);
	//	_delay_ms(1);

	for (i = 0; i < length; i++) {
		i2c_write(data[i]);
		//	_delay_ms(1);
	}
	i2c_stop();
	//	_delay_ms(1);
}

// function for reading 2 consecutive bytes as a word; no error handling
int16_t _i2c_read_word(uint8_t addr) {
	int16_t rtn = 0;
	uint8_t tmp[2]; // 1 word of acceleration

	if (!_mpu_inited) return rtn;

	i2c_read_byte(MPU6050_ADDRESS, addr, tmp);
	i2c_read_byte(MPU6050_ADDRESS, addr+1, tmp + 1);

	rtn = ((tmp[0] << 8) | (tmp[1]));
	return rtn;
}

/* ---
int16_t mpuReadGyroX() - return a signed 16 bit value of the gyroscope's x axis
--- */
int16_t mpuReadGyroX() {
	return _i2c_read_word(MPU6050_RA_ACCEL_XOUT_H);
}

/* ---
int16_t mpuReadGyroY() - return a signed 16 bit value of the gyroscope's y axis
--- */
int16_t mpuReadGyroY() {
	return _i2c_read_word(MPU6050_RA_ACCEL_YOUT_H);
}

/* ---
int16_t mpuReadGyroZ() - return a signed 16 bit value of the gyroscope's z axis
--- */
int16_t mpuReadGyroZ() {
	return _i2c_read_word(MPU6050_RA_ACCEL_ZOUT_H);
}


/* ---
void mpuReadGyro() - read all three axis of the gyroscope into a 3 word buffer
--- */
void mpuReadGyro(int16_t *buff) {
	// read gyro X, Y, Z all at once, high- & low-8-bits combined
	// return int16_t (signed) in buff
	//buff must have at least 3 available places
	//no error handling for too small buff

	if (buff) {
		uint8_t tmp[6]; // 3 words of gyroscopes

		i2c_read_bytes(MPU6050_ADDRESS, MPU6050_RA_GYRO_XOUT_H, 6, tmp);

		buff[0] = (tmp[0] << 8) | (tmp[1]);
		buff[1] = (tmp[2] << 8) | (tmp[3]);
		buff[2] = (tmp[4] << 8) | (tmp[5]);
	}
}

/* ---
int16_t mpuReadAccelX() - return a signed 16 bit value of the acceleration of x axis
--- */
int16_t mpuReadAccelX() {
	return _i2c_read_word(MPU6050_RA_ACCEL_XOUT_H);
}

/* ---
int16_t mpuReadAccelY() - return a signed 16 bit value of the acceleration of y axis
--- */
int16_t mpuReadAccelY() {
	return _i2c_read_word(MPU6050_RA_ACCEL_YOUT_H);
}

/* ---
int16_t mpuReadAccelZ() - return a signed 16 bit value of the acceleration of z axis
--- */
int16_t mpuReadAccelZ() {
	return _i2c_read_word(MPU6050_RA_ACCEL_ZOUT_H);
}

/* ---
void mpuReadAccel() - read all three axis of the gyroscope into a 3 word buffer
--- */
void mpuReadAccel(int16_t *buff) {
	// read accel X, Y, Z all at once, high- & low-8-bits combined
	// return int16_t (signed) in buff
	//buff must have at least 3 available places
	//no error handling for too small buff
	if (buff) {
		uint8_t tmp[6]; // 3 words of accelerations

		i2c_read_bytes(MPU6050_ADDRESS, MPU6050_RA_ACCEL_XOUT_H, 6, tmp);

		buff[0] = ((tmp[0] << 8) | (tmp[1]));
		buff[1] = ((tmp[2] << 8) | (tmp[3]));
		buff[2] = ((tmp[4] << 8) | (tmp[5]));
	}
}


/* ---
void mpuReadOrientation() - *not implemented*
--- */
#if 0
void mpuReadOrientation(int16_t *buff) {
	// pseudo pitch roll yaw
	if (buff) {
		int16_t a[3];
		double x, y, z;
		double pitch, roll, yaw;
		mpu6050_read_accel_ALL(a);
		x = a[0] / 16384.0;
		y = a[1] / 16384.0;
		z = a[2] / 16384.0;
	  	roll  = (atan(       y / sqrt(x*x + z*z )) * 180 / M_PI) - 0.58; // AccErrorX ~(0.58) See the calculate_IMU_error()custom function for more details
  		pitch = (atan((-1 * x) / sqrt(y*y + z*z )) * 180 / M_PI) + 1.58; // AccErrorY ~(-1.58)
		yaw = 0;
#define SIG_DIGITS 1000
		buff[0] = pitch * SIG_DIGITS;
		buff[1] = roll * SIG_DIGITS;
		buff[2] = yaw * SIG_DIGITS;
	}
}
#endif

/* ---
void mpu6050Init() - initialize I2C and then the mpu6050 or mpu9250; returns the MPU's I2C address
--- */
uint8_t mpuInit(void) {
	uint8_t res;

	// power up the I2C pins and the device
	I2C_PWR_DDR |= (1 << I2C_PWR_PIN);
	I2C_PWR_PORT |= (1 << I2C_PWR_PIN);

	_mpu_inited = false;

	i2c_init();

	if (!i2c_start((MPU6050_ADDRESS << 1) | I2C_WRITE))
		_mpu_inited = true;
	else if (!i2c_start((MPU9250_ADDRESS << 1) | I2C_WRITE))
		_mpu_inited = true;

	if (_mpu_inited) {
		//configure important settings in mpu6050
		i2c_write_byte(MPU6050_ADDRESS, MPU6050_RA_PWR_MGMT_1, 0x00);		 //exit sleep mode
		i2c_write_byte(MPU6050_ADDRESS, MPU6050_RA_CONFIG, 0x02);			 // LPF, 0x01 = bandwidth = 184(accel) and 188(gyro); 0x02 = 94 & 98
		i2c_write_byte(MPU6050_ADDRESS, MPU6050_RA_GYRO_CONFIG, 0);			 // gyro ADC scale: 250 deg/s
		i2c_write_byte(MPU6050_ADDRESS, MPU6050_RA_ACCEL_CONFIG, 0x00);		 //accel ADC scale: 2 g
		i2c_write_byte(MPU6050_ADDRESS, MPU6050_RA_INT_ENABLE, 0x00);		 //diable data ready interrupt
		i2c_write_byte(MPU6050_ADDRESS, MPU6050_RA_SIGNAL_PATH_RESET, 0x00); //don't reset signal path

		i2c_write_byte(MPU6050_ADDRESS, MPU6050_RA_PWR_MGMT_1, MPU6050_PWR_TMPOFF); // turn off temperature sensor

		i2c_read_byte(MPU6050_ADDRESS, MPU6050_RA_WHO_AM_I, &res);
		if ((res == MPU6050_ADDRESS) || (res == MPU9250_ADDRESS))
			return res;
		else
			_mpu_inited = false;
	}
	return 0;
}


/* ---
BOOL mpuIsInited() - returns TRUE is the MPU was successfully initialized
--- */
BOOL mpuIsInited() {
	return (_mpu_inited);
}


#endif /* MPU_ENABLED */
