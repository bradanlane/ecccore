/* ************************************************************************************
* File:    leds.h
* Date:    2020.05.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## leds.h - control a set of charlieplexed LEDs using the timer establish in `clock.h`

**Note:** Because this code integrates into the timer established in `clock.h`,
this module must be included before including `clock.h`.

While only a single LED is every ON at any instant, the use of the timer allows for the appearance
of any number of LEDs to be ON at any time. This conserves power and enables the use of charlieplexing
to minimize the number of pins needed for LEDs.

**Tip:** Charlieplexing require 2 pins for 2 LEDs; 3 pins for 6 LEDs; 4 pins for 12 LEDs; 5 pins for 20 LEDs; and 6 pins for 30 LEDs.

--------------------------------------------------------------------------
--- */

#if !LEDS_ENABLED

#define _leds_timer_event() {}
#define ledsInit() {}
#define ledsIsInited() (0)
#define ledOn(num) {}
#define ledOff(num) {}
#define ledBlink(num) {}
#define ledsOn() {}
#define ledsOff() {}
#define ledsSequencer(speed) {}
#define ledsAnimationStart(duration, count, sequence) {}
#define ledsAnimationStop() {}
#define ledsDisable() {}
#define ledsEnable() {}

#else

/*
LEDs are charlieplexed to

  (1) allow additional LEDs in the future with minimal added pins
  (2) prevent multiple LEDs from drawing power at the same time

With a timer running at 2KHz, and a minimum complete refresh rate of 50 times per second, we could handle
  up to 10 LEDs with 4 levels of brightness
  up to 40 LEDs with on/off
*/

#define LED_TIMER_FREQ 2000		// HACK This is actually defined in clock.h but that gets included after leds.h
#define LED_TICKS_PER_MILLI (LED_TIMER_FREQ / 1000)

// NOTE!!! no BRIGHTNESS related code has been provided in favor of on/off performance
#define LED_TICKS (LED_TIMER_FREQ / (LEDS_COUNT) / 50)	// number of ticks to maintain 50Hz refresh for the entire set of LEDs // with 2000 ticks/second and 10 LEDs, this is a refresh rate of 50Hz for every LED

uint8_t _leds_inited;
uint8_t _led_enabled;

#if LED_ANIMATIONS
uint16_t _led_animation_counter;
uint16_t _led_animation_frame_ticks;
uint8_t _led_animation_frame;
uint8_t _led_animation_frames;
uint16_t *_led_animation_sequence;
#endif

/*
LEDs are charlieplexed to

  (1) allow additional LEDs in the future with minimal added pins
  (2) prevent multiple LEDs from drawing power at the same time

  up to 8 LEDs can support 16 levels of brightness
  up to 30 LEDs can support 4 levels of brightness
*/

// using Timer0 for LEDs; TCCR0 is an 8-bit timer

// set output direction and port pin to turn on an LED
// only one LED is on at any time

void _charlieplex_all_off() {
	LED_PORT.DIRCLR = (ALL_LEDS);	// all as input
	LED_PORT.OUTCLR = (ALL_LEDS);	// all low
}

void _charlieplex_led_on(uint8_t led_num) { 	// led_num must be in range for a zero-based index

	//_charlieplex_all_off();
	LED_PORT.DIRSET = _leds_pins[led_num];
	LED_PORT.OUTSET = _leds_out[led_num];
}

// the follow variables are used within the interrupt to change which set of LEDs is active
volatile uint16_t _led_active_num;
volatile uint8_t _led_tick_counter;

#define MAX_STATE_COUNT ((LEDS_COUNT / 8) + 1)
#if (LED_COUNT  <= 16)
volatile uint16_t _leds_state; // one bit per LED
#else
#if ((LED_COUNT  <= 32)
volatile uint32_t _leds_state; // one bit per LED
#else
#error LED_COUNT exceeds 32
#endif
#endif

static inline void _leds_timer_event() {
	if (!_leds_inited || !_led_enabled) return;

	// cycle through charlieplexed LED's really really fast so it looks like any number of LEDs are on at the same time

	if (_led_active_num >= LEDS_COUNT) {
		_led_active_num = 0;
	}

	_led_tick_counter = (_led_tick_counter + 1) % LED_TICKS;
	if (!_led_tick_counter) {
		_charlieplex_all_off();
	#if LED_ANIMATIONS
		// if we are in an animation, we index frame after the specificed number of ticks
		if (_led_animation_frames) {
			if (!_led_animation_counter) {
				_leds_state = _led_animation_sequence[_led_animation_frame];
				_led_animation_frame = (_led_animation_frame + 1) % _led_animation_frames;
			}
			_led_animation_counter = (_led_animation_counter + 1) % _led_animation_frame_ticks;
		}
	#endif
		if (_leds_state & (1<<(_led_active_num)))
			_charlieplex_led_on(_led_active_num);

	_led_active_num++;
	}
}





/* ---
void ledsInit() - called to initialize the LEDs internal state structures and set all LEDs to OFF
--- */
void ledsInit() {
	int i;

	// initialize all of the internal data before starting the interrupt handler
	for (i = 0; i < MAX_STATE_COUNT; i++) {
		_leds_state = 0;
	}

	_led_enabled = true;
	_led_active_num = 0;
	_led_tick_counter = 0;
	_leds_inited = TRUE;
}

/* ---
BOOL ledsInited() - returns TRUE is the LEDs module has been initialized
--- */
BOOL ledsIsInited() {
	return _leds_inited;
}

/* ---
void ledOn(uint8_t num) - turn on an individual LED; it will remain on until turned off directly or when turning off all LEDs
--- */
void ledOn(uint8_t num) {
	if (!_leds_inited) return;
	num = num % LEDS_COUNT;
	_leds_state |= (1 << (num));
}

/* ---
void ledOff() - turn off an individual LED
--- */
void ledOff(uint8_t num) {
	if (!_leds_inited) return;
	num = num % LEDS_COUNT;
	_leds_state &= ~(1<<(num));
}

/* ---
void ledBlink(uint8_t num) - turn on and then off an LED one time very quickly - useful for testing
--- */
void ledBlink(uint8_t num) {
	if (!_leds_inited) return;

	ledOn(num);
	_delay_ms(10);
	ledOff(num);
}


/* ---
void ledsOn() - turn on all LEDs
--- */
void ledsOn() {
	if (!_leds_inited) return;
#ifdef LEDS_CUSTOM_ALL
	_leds_state = LEDS_CUSTOM_ALL;
#else
	_leds_state = -1;
#endif
}

/* ---
void ledsOff() - turn off all LEDs
--- */
void ledsOff() {
	if (!_leds_inited) return;

	_leds_state = 0;
}

/* ---
void ledsMultiOn() - turn on LEDs based on a bit mask
--- */
void ledsMultiOn(uint16_t mask) {
	_leds_state = mask;
}

/* ---
void ledsSequencer(uint8_t speed) - animate the LEDs once time in the sequence defined by the `_leds_order[]` array
--- */
void ledsSequencer(uint8_t speed) {
	int i;

	for (i = 0; i < LEDS_SEQUENCE; i++) {
		ledOn(_leds_order[i]);
		for (uint8_t d = 0; d < speed; d++)
			_delay_ms(50);
	}
	_delay_ms(50);
	for (i = 0; i < LEDS_SEQUENCE; i++) {
		ledOff(_leds_order[(LEDS_SEQUENCE-1) - i]);
		for (uint8_t d = 0; d < speed; d++)
			_delay_ms(50);
	}
}

#if LED_ANIMATIONS
/* ---
void ledsAnimationStart(uint16_t duration, uint8_t count, uint16_t *sequence) - load up an animation defined by the `sequence` array of 16bit values representing the LEDs for each frame and each frame is rendered for teh specified `duration` of CPU *ticks*
--- */
void ledsAnimationStart(uint16_t duration, uint8_t count, uint16_t *sequence) {
	_led_animation_counter = 0;
	_led_animation_frame_ticks = LED_TICKS_PER_MILLI * duration / count / LED_TICKS;
	_led_animation_frames = count;
	_led_animation_sequence = sequence;
	ledsOff();
}

/* ---
void ledsAnimationStop() - end an active animation and return the LEDs to normal state operation using `ledOn()` etc.
--- */
void ledsAnimationStop() {
	_led_animation_counter = 0;
	_led_animation_frame_ticks = 0;
	_led_animation_frames = 0;
	_led_animation_sequence = NULL;
	ledsOff();
}


/* ---
void ledsAnimationSpeed(uint16_t duration) - change speed of active animation
--- */
void ledsAnimationSpeed(uint16_t duration) {
	if (_led_animation_frames) {
		_led_animation_frame_ticks = LED_TICKS_PER_MILLI * duration / _led_animation_frames / LED_TICKS;
	}
}

/* ---
uint_t ledsAnimationFrame() - get current frame (this is a moving target :-O )
--- */
uint8_t ledsAnimationFrame() {
	return _led_animation_frames;
}

#endif


/* ---
void ledsDisable() - enable / disable the entire LED subsystem
--- */
void ledsDisable() {
	_led_enabled = false;
	_charlieplex_all_off();
}

/* ---
void ledsEnable() - enable / disable the entire LED subsystem
--- */
void ledsEnable() {
	_led_enabled = true;
}
#endif // LEDS_ENABLED
