/* ************************************************************************************
* File:    main.c
* Date:    2020.05.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---
# ECCCORE - basic subsystem functions for developing ATMega4809 projects

This code was originally developed for the creation of ECC projects.
However, the code is useful in a range of projects which use the ATMega4809 8bit microchip.
The code is very compact and does not require any Arduino or other external libraries.

**ECC** = eChallengeCoin, eChallengeCard, eChallengeCube, eChallengeCar, eChallengeCub, etc.

The ECC projects consist of a configurable core and a series of configurable challenges in the form
of interactive games, story puzzles (access through a UART connection), and various demos.

The challenges are unique to each ECC and to publish that code would be SPOILERS :-)

To accommodate the desire to share as much as possible, the core code is public under the MIT license and
used as a GIT submodule within the specific ECC projects.

**Project Notes:**

The ECC projects uses an electronic schematic resembling a stripped down Arduino. There is no external oscillator,
and the power is a single 3V CR2032 or equivalent source. The CPU is run at 2 MHz and the UART operates at 9600,N,8,1

A typical ECC has a series of LEDs, a series of touch sensors, and UART communications.
There is also support for a piezo buzzer, MPU gyro accelerometer, and SPI FLASH.

The core supports the AVR POWER_DOWN sleep mode to conserve power. By default, the mode is activated after 60 seconds of inactivity.

Not all core features are used by all ECC projects. Not all core features are tested by default from the sample code in `main.c`.

**Caution:** The MPU and SPI FLASH code was written for the ATMega328PB and has not bee ported forward to the ATMega4809.

--------------------------------------------------------------------------
--------------------------------------------------------------------------

--- */

///////////////////////////////////////////////////////////////////////////////////////////////////

// this must be included first
#include <avr/power.h>

// current implementation uses the internal reference at 2Mhz to make 9600 baud serial more stable
#define F_CPU 2000000UL // define it now as 2 MHz unsigned long

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h> // NOTE: with the ATMega4809, PROGMEM is a NOP
#include <avr/sleep.h>
//#include <avr/boot.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <util/atomic.h>
#include <util/delay.h>


// when using this code as a submodule in another project, create a config.h file with the entries of config.h and expand as needed
#include "config.h"
#include "helperdefs.h" // this must be included second
#include "leds.h" 		// the leds must be included before the clock to enable the inline code
#include "clock.h"		// will call for leds, if configured
#include "uart.h"
#include "touch.h"
#include "sound.h"
#include "random.h"
#include "power.h"
#include "eeprom.h"
#include "buttons.h"
#include "flash.h"
#include "mpu.h"

#include "smoketest.h"

static void init_cpu() {
	// FUSE 2 is OSCCFG and 0x00=20Mhz 0x01= 16Mhz
	// set internal oscillator to 16Mhz and then DIV8 to get 2Mhz

	// set internal oscillator to default (20Mhz) and then DIV10 to get 2Mhz

	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, (CLKCTRL_PDIV_10X_gc | CLKCTRL_PEN_bm));
	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLA, (!CLKCTRL_CLKOUT_bm)); // set to 16MHz requires fuse change: PortaProg = echo 'set 2 01'
															   //_PROTECTED_WRITE(CLKCTRL.MCLKCTRLA, (!CLKCTRL_CLKOUT_bm | CLKCTRL_CLKSEL_OSC20M_gc));	// set to 20MHz
															   //_delay_ms(200);
	_delay_ms(100); // give the system time to warm; eliminates a startup lockup problem
}

///////////////////////////////////////////////////////////////////////////////////////////////////

/* ---
main() - a non-comprehensive example using portions of the ECC CORE functionality. (examine the code for more details)

Here are just a few useful notes:
--- */
int main(void) {
	init_cpu();

	// the order of the subsystem initialization is significant
	clockInit(); // must be initialized first
	uartInit();
	powerInit();
	randomInit();
	ledsInit();
	soundInit();
	touchInit();
	eepromInit();
	buttonInit();

	smoketest();

	return (0); // should never get here, this is to prevent a compiler warning
}
