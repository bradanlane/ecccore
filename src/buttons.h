/* ************************************************************************************
* File:    button.h
* Date:    2020.10.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## button.h - simple button handling functions

This code is written for the ATMega4809.

A button is defined by a port and a pin.
The code is simple and assumes a button is pressed when its value is LOW.
Buttons are referenced by their index in teh button table.
For code readability, names may be given to the indexes - *as detailed on `config.h`.

**NOTE:** the button code does not support detecting short vs long press.
However, this is a logical and realtively simple extension.
--------------------------------------------------------------------------
--- */

#if !BUTTONS_ENABLED

#define buttonInit() {}
#define buttonPressed(n) (false)

#else

void buttonInit() {
	for (int i = 0; i < BUTTON_COUNT; i++) {
		(_button_ports[i])->DIRCLR = _button_pins[i];	// make button pin as input
		// enable internal pullup
		*(_button_ctrls[i]) |= PORT_PULLUPEN_bm; // has internal pull-up
	}
}

BOOL buttonPressed(n) {
	// illegal buttons can not be pressed
	if (n >= BUTTON_COUNT)
		return false;

	if (~((*_button_ports[n]).IN) & _button_pins[n]) /* check if button pulled to GND */ {
		return true;
	}
	return false;
}

#endif // end BUTTONS_ENABLED
