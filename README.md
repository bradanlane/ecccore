# ECCCORE - basic subsystem functions for developing ATMega4808/4809 projects

This code was originally developed for the creation of ECC projects.
However, the code is useful in a range of projects which use the ATMega4809 8bit microchip.
The code is very compact and does not require any Arduino or other external libraries.

**ECC** = eChallengeCoin, eChallengeCard, eChallengeCube, eChallengeCar, eChallengeCub, etc.

The ECC projects consist of a configurable core and a series of configurable challenges in the form
of interactive games, story puzzles (access through a UART connection), and various demos.

The challenges are unique to each ECC and to publish that code would be SPOILERS :-)

To accommodate the desire to share as much as possible, the core code is public under the MIT license and
used as a GIT submodule within the specific ECC projects.

**Project Notes:**

The ECC projects uses an electronic schematic resembling a stripped down Arduino. There is no external oscillator,
and the power is a single 3V CR2032 or equivalent source. The CPU is run at 2 MHz and the UART operates at 9600,N,8,1

A typical ECC has a series of LEDs, a series of touch sensors, and UART communications.
There is also support for a piezo buzzer, MPU gyro accelerometer, and SPI FLASH.

The core supports the AVR POWER_DOWN sleep mode to conserve power. By default, the mode is activated after 60 seconds of inactivity.

Not all core features are used by all ECC projects. Not all core features are tested by default from the sample code in `main.c`.


--------------------------------------------------------------------------
--------------------------------------------------------------------------


main() - a non-comprehensive example using portions of the ECC CORE functionality. (examine the code for more details)

Here are just a few useful notes:

# ECCCore Smoketest - basic subsystem test functions for an ECCCore based PCB

### OPERATING PRINCIPLE

The smoketest exercises each subsystem in series:

- This example requires user interaction to verify the LEDs and buzzer are working.
- The touch sensors are tested for any gross out-of-range values and then
- the sensor values are read and stored as the baseline values - eg calibration.
- The UART test assumes an external process sends a predefined string.
- The IR test assumes there the IR LED is optically looped back to the phototransistor.

### PORTAPROG INTEGRATION

The smoketest can work with any external test infrastructure capable of displaying the content
send from the smoketest over UART and capable of sending one or more predetermined text strings
which are validated by the smoketest code.

The code in this file was designed to work against a script running on a **PortaProg** portable programmer and test device.
The associated **PortaProg** test script is `smoketest.cmd` (provided with this repository).

The general overview of the `smoketest.cmd` script:

- sets the programmer to UPDI mode,
- writes the current firmware to the ECC device,
- powers the ECC device on (which begins the smoketest code),
- waits 5 seconds to receive a message containing the text 'BUZZER',
- sends the text 'UART' (which this smoketest code is looking for),
- waits for 6 seconds to receive a message containing the text 'SMOKETEST'
- which occurs at the end of the smoketest.

--------------------------------------------------------------------------
--------------------------------------------------------------------------


smoketest() - a comprehensive (?) test of the PCB to insure the build is successful


# config_m4809.h - define all configuration for the specific ECC project using the ATMega4809 microcontroller

All of the ECC subsystems are configured from this one file. It is important to match the definitions
below with the electrical schematic for all used IO Pins.

## Config Notes:

The subsystems use various AVR timers. Those are not defined here but
probably have serious implications if project level code will need to use them. _Sorry._

The original ECCCore was built for the ATMega328PB. The intent was to make it generic and have
all configuration driven the a file like this one. Well, that was a good idea and if this project
had hundreds of users, it would be be warranted. However, it is really only used by Bradán Lane STUDIO
so the decision was to fork the ECCCore with the switch to ATMega4809. _Sorry (again)._

The ATMega4809 Atmel syntax for pins and port operations provides both bit numbers (bp) and bit masks (bm).
This implementation of ECCCore now uses bit masks to simplify code. *Ironically, Atmel is inconsistent
within the `iom4809.h` using HEX mask values in some cases and using bit-shift macros in other cases.*

Example: `PORTA.OUT |= (1 << PIN0_bp);` is the same as `PORTA.OUTSET = PIN0_bm;`

--------------------------------------------------------------------------

### Subsystem Enablement:
Most subsystems are optional and may be included/excluded as appropriate for the specific project.

#### EEPROM_ENABLED
EEPROM is the persistent storage of the microcontroller.

#### IR_ENABLED
The I/O UART is implemented on UART0. The IR transceiver uses a second UART.

#### LEDS_ENABLED
LEDs are charlieplexed and use a timer. Even if LEDs are not enabled, the timer is still used to provide
a clock source for measuring events. Timer TCB0 is used.

#### TOUCH_ENABLED
TOUCH sensors measure the relative capacitance on an IO pin. All sensors are updated in a single synchronous function.

#### SOUND_ENABLED
The sound subsystem implements a one octave music scale consisting of 12 notes. The sound subsystem
assumes a piezoelectric buzzer on a specific pin to allow for using a hardware timer.

#### BUTTONS_ENABLED
The buttons subsystem provides convenience functions for checking the state of one or more buttons.

#### FLASH_ENABLED
storage uses the SPI interface and a CS pin.

#### MPU6050_ENABLED
The MPU6050 uses the I2C interface. The AVR specific pins are used and must have the requisite pull-up resistors.

#### LED_ANIMATIONS
There is optional support for animations. It is optional since each animation requires (LED_COUNT+1) 16bit values.

An animation consists of frame duration and a bit mask for each frame.
The duration is the amount of time for one complete cycle of the animation (in milliseconds).

**Hint:** Use the `ecc_animation.xlsx` spreadsheet to calculate the LED masks!

#### SLEEP_MESSAGE_ENABLED
It can be handy to display a message when entering sleep mode and when exiting sleep mode.

### Fixed Purpose Pins:
Very few pins of the ATMega4809 have explicit meaning but several have default capabilities
and are used within the ecccore. It is important to uses these pins appropriately within
the project schematic. They are included here to make it clear not to use them
for other purposes.

The UPDI pin must not be used.

The USARTs on the ATMega4809 uses PIN0 and PIN1 on PORTA, PORTC, PORTF, and PORTB corresponding to USART0 .. USART3.
Each USART also supports an alternate pin assignment using PIN4 and PIN5 on the same ports.
The code in `uart.h` assume the primary pin assignments are used.


The wake from sleep uses ???? (pin PA6)

The sound subsystem uses hardware timer

The MPU6050 uses an I2C interface on I2C (pin PA2 and PA3); there are alternate pins for I2C (pin PC2 and PC3)

**Synopsis:** At this point, we have used ???? pins

### User Define Power Output Pins:
To conserve power during sleep, all attached peripherals must also be powered down.
A simple solution is to power them from GPIO pins. This assumes the peripheral power
requirements do not exceed the output of an GPIO pin.

If a peripheral is only needed for a short time, vs anytime the device is awake, then
that peripheral should have it's own pin.

The touch sensors use power to charge the TOUCH during the measurement cycle. All touch sensors use a single power source.

The MPU6050 requires power as does the I2C pull up resistors

The FLASH storage chip requires power.

### User Defined IO Pins:
Several general IO pins are needed within the core. It is important to match the definitions with the project schematic.

The IR circuit forms a hardware level UART. The photo-transistor requires power for its pull up.

While SPI is defined by the AVR, when SPI is shared with other devices, those devices each require a CS pin.

An ECC project may have buttons or interactions connected to pins.
Each button requires a port and a pin assignment. Currently all buttons are assumed pressed when LOW.

### LED IO Pins:
The LEDs are charlieplexed to reduce power usage and pin usage. The code assumed all LED pins are within a single IO PORT.
Charlieplexed LEDs require 2 pins for 2 LEDs; 3 pins for 6 LEDs; 4 pins for 12 LEDs; 5 pins for 20 LEDs; and 6 pins for 30 LEDs.

There are many different ways to define the LEDs. The minimum definitions and declarations
include: `LEDS_COUNT`, `ALL_LEDS`, `LED_DDR`, `LED_PORT`, and the `_leds_pins[]`, `_leds_out[]`, & `_leds_order[]` arrays.

**Hint:** If you find you are mixing up the order of the LEDs because of the bidirectional nature of the pins and charlieplexing
then you may find it helpful to define each LED's pins and direction and then use those to initialize the data arrays.

### Touch Sensor IO Pins:
Touch sensors require a specific project schematic and PCB design.

The touch sensing is performed synchronously.

With only a single PORT, the maximum number of touch sensors is 8, but this only is possible when using PORT B and not using any conflicting AVR capabilities.
With PORT C or D, the maximum number of touch sensors is 6.

The code had been expanded - *and made a bit more complex* - to handle touch sensors across two AVR PORTS. It would be smaller code and
slightly more efficient if all sensors were within a single AVR PORT. When `TOUCH_PORTS_2X` is ZERO, the code will optimize.

There are many different ways to define the Touch sensors. The minimum definitions and declarations include:
- `TOUCH_PORT_n?`
- `TOUCHn_PIN`
- `TOUCH_COUNT`
- `TOUCH_SPLIT` (when two ports are used)

The calibration of the touch sensors is configured using either:
`TOUCH_THRESHOLD`
`TOUCH_READINGS[]` (array of touch readings used for individual sensor calibration)

If all touch sensors have the same raw reading, then the `TOUCH_THRESHOLD` may be used.
Alternately, if a test of the touch sensors - when no contact is made - shows different values for each then
the `TOUCH_READINGS[]` array may be set with those readings. The code then uses the readings as abaseline and
applies `TOUCH_DELTA` to each to determine touch activation. _(The touch readings are reported by the demo code in `main()`)_

**NOTE1:** It is rare that `TOUCH_MEASUREMENT_ITERATIONS` or `TOUCH_UPPER_LIMIT` need to be changed but there may
be cases where the touch range needs greater latitude.

**NOTE1:** The threshold and readings are based on the time it takes the code to test and loop.
For this reason, the values are affected by the MCU clock speed `F_CPU`. The initial values provided  here are based on tests
with a MCU running at 2MHz.


**Tip:** It is not used by the core code but it is helpful for project development to create a series of bit masks for combinations of
touch sensors which will be used to have specific meanings. The most common is `TOUCH_ALL` but many other combinations could be created.
By creating bit maps of the combinations, test cases are very efficient.

### EEPROM Setup:
No specific configuration setup is *required* for the EEPROM. However, for `format` detection to work, a user defined bytecode
and location is required. These are optional definitions and the code will optimize away format detection if these are not defined.

If the `FORMAT_MARKER` definitions are present, the eeprom *format* related functions are useful for storing initial values in EEPROM.

*Note:* The ATMega4809 has only 256 bytes of EEPROM and the functions take use a relative address space starting at 0x0000

### FLASH Setup:
No specific configuration setup is *required* for the FLASH. However, for `format` detection to work, a user defined bytecode
and location is required. These are optional definitions and the code will optimize away format detection if these are not defined.

If the `FORMAT_MARKER` definitions are present, the eeprom *format* related functions are useful for storing initial values in FLASH.

### Power Idle Timeouts:
The power-down sleep capability can act on idle detection. There are twe functions to quickly indicate interaction and reset the idle timeout.
The `NORMAL_SLEEP_DELAY` and `LONG_SLEEP_DELAY` definitions are used to control defaults for idle timeout.

### SLEEP_MESSAGE Setup:
The sleep / wake / WARNING messages are not required. If any of the the `SLEEP_MESSAGE`, `WAKE_MESSAGE`, or  `WARNING_MESSAGE` definitions and not set, the corresponding code will be optimized away.

### UART Buffers:
**Assumptions:** The recipient of transmitted data is fast enough that outbound data rarely blocks on queuing data into the TX buffer.
The receive buffer often needs to be larger to accommodate incoming data when other ECC activity delays pulling data off the RX buffer.
When also using the IR UART, it is not necessary they have the same buffer sizes if their purpose if significantly different.



## leds.h - control a set of charlieplexed LEDs using the timer establish in `clock.h`

**Note:** Because this code integrates into the timer established in `clock.h`,
this module must be included before including `clock.h`.

While only a single LED is every ON at any instant, the use of the timer allows for the appearance
of any number of LEDs to be ON at any time. This conserves power and enables the use of charlieplexing
to minimize the number of pins needed for LEDs.

**Tip:** Charlieplexing require 2 pins for 2 LEDs; 3 pins for 6 LEDs; 4 pins for 12 LEDs; 5 pins for 20 LEDs; and 6 pins for 30 LEDs.

--------------------------------------------------------------------------

void ledsInit() - called to initialize the LEDs internal state structures and set all LEDs to OFF

BOOL ledsInited() - returns TRUE is the LEDs module has been initialized

void ledOn(uint8_t num) - turn on an individual LED; it will remain on until turned off directly or when turning off all LEDs

void ledOff() - turn off an individual LED

void ledBlink(uint8_t num) - turn on and then off an LED one time very quickly - useful for testing

void ledsOn() - turn on all LEDs

void ledsOff() - turn off all LEDs

void ledsSequencer(uint8_t speed) - animate the LEDs once time in the sequence defined by the `_leds_order[]` array

void ledsAnimationStart(uint16_t duration, uint8_t count, uint16_t *sequence) - load up an animation defined by the `sequence` array of 16bit values representing the LEDs for each frame and each frame is rendered for teh specified `duration` of CPU *ticks*

void ledsAnimationStop() - end an active animation and return the LEDs to normal state operation using `ledOn()` etc.

void ledsAnimationSpeed(uint16_t duration) - change speed of active animation

void ledsDisable() - enable / disable the entire LED subsystem

void ledsEnable() - enable / disable the entire LED subsystem


## touch.h - digital touch sensor measurements

The touch sensing is based on measuring a capacitance change between a conductive
sensor created on the PCB and earth (self capacitance sensor).

The code measures the time for a pin to go from LOW to HIGH. When no capacitance is present,
the time is short. The time increases with the increase in capacitance.

The human body will act as a capacitor. So it is possible to detect with the sensor
is touched because the measure time increases.

Using a threshold, the capacitance touch sensor may be used as a momentary contact button.

A small sensor area will work with no isolation layer - aka without solder mask. However, there is the
risk of static discharge into the pin.

The sensor pin is pulled HIGH through a large resistor (1M Ohm or larger). The sensor pin is
then momentarily pulled low. The number of cycles of a tight loop is measured until the sensor pin is HIGH again.
The more capacitance present, the longer it takes for the pin to be HIGH.

It is possible to use VCC as the *always on* HIGH state. However, this implementation uses
another pin as the _always on_ HIGH state. Using a pin as the power source allows the code to
disable the power pin to prevent a vampire power drain when the MCU is in sleep power down mode.

The threshold value(s) are determined by the combination of the chosen resistor size,
the size of the touch pad on the PCB, and the presence or absence of solder mask.
It is possible that multiple sensors on a PCB will each have different thresholds.
The settings in `config.h` provides for this possibility.

**Note:** It is highly advisable to read all of the comments within the module to understand
the function of the circuit and its relation to the code.

--------------------------------------------------------------------------

void touchClear() - clear all data associated with the touch sensors

uint16_t touchTouching() - compute the values for all touch sensors and return their state as a bit field (1 bit per sensor)

uint16_t touchTouched() - compute the values for all touch sensors and return their state as a bit field (1 bit per sensor) then clear the sensors

uint8_t touchValue(uint8_t num) - fetch the current numerical measurement for a single sensor (this is only valid after touchTouching())

void touchInit() - called to initialize the touch sensor state structures

BOOL touchInited() - returns TRUE is the touch module has been initialized

BOOL touchCalibrate() - calibrate the touch sensors and save results to eeprom

uint8_t touchValue(uint8_t num) - fetch the current numerical measurement for a single sensor (this is only valid after touchTouching())


## sound.h - single musical note sound generator

This code is written for the ATMega4809. However, within the comments section you will find
the necessary timer values for the tinyAVR 1-series.

The frequency table is the musical scale starting at C3.

The megaAVR timer is 16bit to support multiple octaves of notes.

The macros `N`, `D`, and `T` are for creating music and sound files with a *packed byte* data format.
--------------------------------------------------------------------------

void soundOff() - turn off sound and disable the timer

void soundFreq() - turn on sound of frequency by setting up the timer for hardware output on the `BUZZER_PIN`

NOTE: this function does not attempt to stop the current sound when setting a new one

void soundOn() - turn on sound of musical note by setting up the timer for hardware output on the `BUZZER_PIN`


## flash.h - format, read/write interface to SPI FLASH storage

The module provides basic formatting, read, and write to SPI connected FLASH storage.

The code was developed and tested with the WINBOND FLASH Chip: W25Q64BVSIG (older cheaper slower). This chip has 
8MB of total storage broken into 256Byte pages. The chip is access using 4KB sectors. The chip is formatted either in 
its entirety or at the 64KB block level or 4KB sector level.

**Note:** To overwrite a page the entire sector (or block or chip) must be erased first.

**Warning:** This code requires a 256 byte buffer for page level operations.
The code establishes `_flash_page_buffer` as a local buffer. To save data space, 
it is recommended to treat this as a read-only buffer within your code.

--------------------------------------------------------------------------

BOOL flashWriteData(uint16_t block, uint16_t page, uint16_t index, uint8_t *buffer, uint16_t size) - 
write data to a page of FLASH storage at the specified block+page+index address. The size of the write must be 
less than a full page - eg 256 bytes - and most not attempt to write across a page boundary.

The BLock+Page+Index address indicates a 64 KB block, a 256 Byte Page within that block, and then an index (usually 0) withing that page.

*Kludge:* the write seems to have a timing issue beyond about 238 bytes

int16_t flashReadData(uint16_t block, uint16_t page, uint16_t index, uint8_t *buffer, uint16_t size, BOOL just_text) - 
read data from a page of FLASH storage from the specified block+page+index address. The size of the read must be 
less than a full page - eg 256 bytes - and most not attempt to read across a page boundary.

The Block+Page+Index address indicates a 64 KB block, a 256 Byte Page within that block, and then an index (usually 0) withing that page.

The `just_text` flag indicates that the read operation should stop if it encounters an uninitialized byte.
Otherwise, it will continue to read into the supplied buffer for the requested number of bytes.

The functions returns the number of bytes read.

int16_t flashReadByte(uint16_t block, uint16_t page, uint16_t index) - 
read a single byte from a page of FLASH storage from the specified block+page+index address.

The Block+Page+Index address indicates a 64 KB block, a 256 Byte Page within that block, and then an index (usually 0) withing that page.

The functions returns the byte.

BOOL flashFormat() - 
erase  the entire FLASH storage chip and optionally write a marker to indicate it has been *formatted*.

The functions returns TRUE if the erase was successful.

BOOL flashIsFormated() - 
The functions returns TRUE if the `FLASH_FORMAT_MARKER_BYTE` is detected.

**Note:** if the `FLASH_FORMAT_MARKER_BYTE` was not defined, then this function always returns true.
It is up to the user to perform a chip erase as needed.

BOOL flashInited() - returns TRUE if the flashInit() function has already been called and it was successful.

BOOL flashWasFormatted() - returns TRUE if the FLASH was formatted during the current boot/execution

void flashInit() - initialization of the FLASH SPI interface and optionally erase and format the FLASH if it is determined to be unformatted.


## mpu.h - read gyroscope and acceleration data from an MPU6050 or MPU9250 I2C device

This module provides basic access to read the data for the 3-axis gyro + 3-axis accelerometer of the IMU.

**Disclaimer:** This code is really gorpy and is derived from the work of Yifan Jiang.

--------------------------------------------------------------------------

int16_t mpuReadGyroX() - return a signed 16 bit value of the gyroscope's x axis

int16_t mpuReadGyroY() - return a signed 16 bit value of the gyroscope's y axis

int16_t mpuReadGyroZ() - return a signed 16 bit value of the gyroscope's z axis

void mpuReadGyro() - read all three axis of the gyroscope into a 3 word buffer

int16_t mpuReadAccelX() - return a signed 16 bit value of the acceleration of x axis

int16_t mpuReadAccelY() - return a signed 16 bit value of the acceleration of y axis

int16_t mpuReadAccelZ() - return a signed 16 bit value of the acceleration of z axis

void mpuReadAccel() - read all three axis of the gyroscope into a 3 word buffer

void mpuReadOrientation() - *not implemented*

void mpu6050Init() - initialize I2C and then the mpu6050 or mpu9250; returns the MPU's I2C address

BOOL mpuIsInited() - returns TRUE is the MPU was successfully initialized


## clock.h - a 1 millisecond reference timer value

The clock module uses TCB0 to establish a 2KHz timer and exposes it with 1 millisecond
accuracy. This is useful for performing timed operations, animations, detecting timeouts, etc.

In addition to being a simple instrument for timing things, the clock module also
handles the clarlieplexed LEDs if/when they are enabled and initialized.

**Note 1:** 2Khz was chosen over 10Khz because the amount of time needed to perform the
interrupt task - both incrementing the clock and handling the LEDs.
The time to complete the interrupt affects the ultimate frequency. This may either
be tolerated or the compare/capture value adjusted to compensate.
Using an oscilloscope, a clock compensation value was determined.
It should be noted that this method is not perfect as the 'observer effect' may have effected the measurement
and thus the compensation value. Life sucks sometimes but you gotta do what you gotta do.

**Note 2:** When used in conjunction with LEDs, the led module must be included before the clock module.

--------------------------------------------------------------------------

void clockInit() - called to initialize the clock; this function should be called very early in the program execution

uint32_t clockMillis() - return the current counter as a 32bit unsigned integer. The counter starts at 0 when clockInit() is first called.


## eeprom.h - read/write interface to ATMega4809 eeprom memory

The module provides basic read/write of the microcontroller eeprom.

**Tip:** Writing a specific bytecode to a specific location of the EEPROM is an easy way to know if
it has been used before or if initial data should be written. If the bytecode is missing, then it can be
considered "unformatted" and any initial data should be written.
Alternately, you could test the values of every read operation.
See the `config.h` for additional details.

--------------------------------------------------------------------------

uint8_t eepromIsReady() - returns TRUE if the eeprom is ready and FALSE if it busy

void eepromWriteByte() - write a single byte to an eeprom memory location

void eepromReadByte() - read a single byte from eeprom memory relative to the eeprom base address

void eepromFormat() - set a marker byte to indicate the EEPROM has a known default state

BOOL eepromWasFormatted() - return TRUE if the eeprom was formatted during the current boot/execution

BOOL eepromInited() - return TRUE when the EEPROM is ready for use

void eepromInit() - initialization of the EEPROM functions and conditionally format

uint8_t* eepromSignature() - returns a pointer to a static buffer with our version the MCU unique ID

int eepromAddID() - store a chip ID but only if it has not already been stored


## power.h - sleep / wake functions

**Note:** Because this code uses the timer established in `clock.h`,
this module must be included after including `clock.h`.

The power module provides sleep and wake capability as well as idle timeout detection.
It preserves all registers and timers during the sleep cycle.
The wake is detected from INT0.

--------------------------------------------------------------------------

void powerInit() - perform necessary initialization of the power management subsystem

void powerButtonSleepEnable/Disable() - allow sleep mode to be activated using the wake button

void powerSleepUdate() - signal user action has occurred and reset the idle timeout

void powerSleepUdateLong() - signal user action has occurred and reset the idle timeout for a long period

void powerSleepNow() - begin sleep power shutdown process immediately; wake is triggered by INT0 being pulled low

void powerButtonPressed() - expose the wake/sleep button so it is available for other uses.
A common use will be to skip a long process (such as a game puzzle output).

void powerSleepConditionally() - begin sleep power shutdown process if the idle timer has expired.
This function should be called periodically from the main loop.


## random.h - pseudo random number generated

The random module provides a convenient source of pseudo random numbers. The generator is seeded from the noise of the internal ADC.

**Warning:** The seed is generated from the ADC using the temperature sensor.
If the ADC is high, then the value will always be 1023. Try altering the prescaler.

--------------------------------------------------------------------------

void randomInit() - initialize and seed the pseudo random number generator

uint8_t randomGetNum(max) - get an 8 bit  pseudo random number between zero and max

uint8_t randomGetByte() - get a pseudo random byte value between zero and 0xFF

uint16_t randomGetWord() - get a 16 bit pseudo random number

uint16_t randomGetSeed() - get the active pseudo random seed value

